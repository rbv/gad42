if __name__ == "__main__":
    import paths

import os.path
from itertools import chain
import pygame
import gfx
from gfx import Screen
from pygame.locals import Color, RLEACCEL
from gummworld2 import State
from sentence import *


#######################  TEXT RENDER  ##########################################

font_dir = os.path.join(os.path.dirname(__file__), "font")

#default_font = pygame.font.Font(data.filepath('font', 'Vera.ttf'), 15)
#default_font.set_bold(True)
#default_font = pygame.font.Font(data.filepath('font', 'monoMMM_5.ttf'), 13)
default_font = pygame.font.Font(os.path.join(font_dir, 'Terminus.ttf'), 16)
small_font = pygame.font.Font(os.path.join(font_dir, 'Terminus.ttf'), 14)
emph_font = pygame.font.Font(os.path.join(font_dir, 'Terminus.ttf'), 16)
emph_font.set_bold(True)

hud_alpha = 208
#default_colour = Color(50, 60, 230)
#default_colour = Color(20, 240,70)
default_colour = Color(0,210,1)
antialias_font = False


# The following line wrapping functions are originally from the pygame wiki, author anonymous
# http://www.pygame.org/wiki/TextWrapping
 
def truncline(text, font, maxwidth):
    real=len(text)       
    stext=text           
    l=font.size(text)[0]
    cut=0
    a=0                  
    done=1
    old = None
    while l > maxwidth:
        a=a+1
        n=text.rsplit(None, a)[0]
        if stext == n:
            # No newlines, trim characters from the end instead
            cut += 1
            stext= n[:-cut]
        else:
            stext = n
        l=font.size(stext)[0]
        real=len(stext)               
        done=0                        
    return real, done, stext             
        
def wrapline(text, font, maxwidth):
    done=0                      
    wrapped=[]                  
    firststep = True
    while not done:
        nl, done, stext=truncline(text, font, maxwidth) 
        if not firststep:
            stext = stext.lstrip()
        firststep = False
        wrapped.append(stext)
        text = text[nl:]                                 
    return wrapped
 
 
def wrap_multi_line(text, font, maxwidth):
    """ returns text taking new lines into account.
    """
    lines = chain(*(wrapline(line, font, maxwidth) for line in text.splitlines()))
    return list(lines)

class WrappedText(object):
    # Anchors
    LEFT = 0
    CENTRE = -0.5
    RIGHT = -1

    def __init__(self, *args, **kwargs):
        self.rendered = None
        self.colour = default_colour
        self.font = default_font
        self.darkenings = 0
        self.override_width = None
        self.rect = pygame.Rect(0,0,800,600)
        #self.rect = None
        if len(args) or len(kwargs):
            self.set(*args, **kwargs)

    def set(self, text, colour=None, font=None, width=None, anchor=LEFT):
        assert isinstance(text, str)
        self.text = text
        if font: self.font = font
        if colour:
            if isinstance(colour, Color):
                self.colour = colour
            elif isinstance(colour, tuple):
                self.colour = Color(*colour)
            else:
                self.colour = Color(colour)
        if width: self.override_width = width
        self.anchor = anchor
        self.rendered = None

    def darken(self):
        if self.darkenings < 4:
            self.darkenings += 1
            self.colour = gfx.darken_colour(self.colour, 0.87)
            self.rendered = None

    def set_dest_width(self, rect_or_w = None):
        oldrect = self.rect
        if hasattr(rect_or_w, 'width'):  # a rect
            self.rect = rect_or_w
        else:
            self.rect = pygame.Rect((0, 0), (rect_or_w, 600))
        if not oldrect or oldrect.width != self.rect.width:
            self.rendered = None

    def set_dest(self, surface, rect=None):
        self.dest = surface
        if surface and rect == None:
            self.set_dest_width(surface.get_clip())
        else:
            self.set_dest_width(rect)

    def _render(self):
        #if self.rect.width:
        lines = wrap_multi_line(self.text, self.font, self.rect.width)
        #else:
        #    lines = (self.text,)
        self.rendered = []
        for line in lines:
            self.rendered.append(self.font.render(line, antialias_font, self.colour))
        self.true_width = max([0] + [surface.get_rect().width for surface in self.rendered])

    def height(self):
        if self.rendered == None:
            self._render()
        return self.font.get_linesize() * len(self.rendered)

    def width(self):
        if self.override_width is not None:
            return self.override_width
        if self.rendered == None:
            self._render()
        return self.true_width + 5

    def draw(self, surface=None, rect=None):
        """Optionally specifies surface/rect, otherwise uses remembered surface if possible
        Returns the remaining dest rect available"""
        if surface or rect:
            self.set_dest(surface, rect)
        if self.dest == None:
            raise Exception("WrappedText.draw without destination!")
        if self.rendered == None:
            self._render()
        destrect = self.rect.move(self.true_width * self.anchor, 0)
        for drawnline in self.rendered:
            ##print "drew TEXT at " + str(destrect)
            self.dest.blit(drawnline, destrect)
            destrect.y += self.font.get_linesize()
        return destrect


#######################  CONSOLE  ##########################################

def console(*args, **kwargs):
    State.console.add(*args, **kwargs)

class Console(object):
    """The message log shown in the HUD.

    NOTE: the message history menu accesses .messages directly. The strings
    will be wrapped when this happens"""

    def __init__(self):
        self.rect = pygame.Rect(180, 0, 600, 80)
        #self.font = pygame.font.Font(data.filepath('font', 'Vera.ttf'), 15)
        self.font = default_font
        self.scrollback = 100
        self.messages = []
        self.newline = True
        self.do_darken = False

    def time_step(self):
        "Darken old text when next string is added"
        self.newline = True
        self.do_darken = True

    def new_context(self):
        global context
        self.newline = True
        context = []
        
    def add(self, *msg_parts, **kwargs):  #colour = Color('green')):
        entity = kwargs.get("entity")
        newparts = []
        for part in msg_parts:
            if hasattr(part, "check_visible"):  #isinstance(part, Entity):
                entity = part
                break
        if entity and not entity.check_visible():
            #print "Hidden:", msg_parts
            return
        colour = kwargs.get("colour") or Color('green')
        msg = form_msg(*msg_parts)
        if self.do_darken:
            self.do_darken = False
            for message in self.messages:
                message.darken()

        lastline = None
        if len(self.messages) > 0:
            lastline = self.messages[-1]
            if lastline.colour != colour:
                # Can't switch colour in the middle of a line
                self.newline = True
        if self.newline:
            if len(self.messages) >= self.scrollback:
                self.messages[0:1] = []
            self.newline = False
            textobj = WrappedText(msg, colour, self.font)
            #textobj.set_dest(Screen.surface, self.rect)  # set dest width
            self.messages.append(textobj)
        else:
            #append
            oldmsg = lastline.text.rstrip()
            if oldmsg[-1] not in (".", "!", "?", ","):
                oldmsg += ".  "
            else:
                oldmsg += "  "
            lastline.set(oldmsg + msg)
            
    def draw(self, rect = None):
        if rect:
            boxrect = rect
        else:
            boxrect = self.rect
        destrect = boxrect.copy()
        destrect.y += destrect.height
        screen = Screen.surface
        old_clip = screen.get_clip()
        ##print "clipping to " + str(self.rect)
        screen.set_clip(boxrect)
        for msg in reversed(self.messages):
            # Set dest before querying height
            msg.set_dest(screen, destrect)
            height = msg.height()
            destrect.y -= height
            msg.draw()
            if destrect.y <= boxrect.y:
                break
        screen.set_clip(old_clip)
