import copy
import heapq
from random import randint, uniform
import pygame
from text import *
from pygame.locals import *
import gfx
import sfx
from maps import Tile
from skills import *
from misc import *
from gummworld2 import State, Vec2d


class Stats(object):
    # Normal stats
    hp = 1
    maxhp = 1
    mp = 0
    maxmp = 0
    stam = 0
    maxstam = 0
    str = 0
    dex = 0
    con = 0
    psy = 0
    AC = 0
    exp = 0

    # Other stats
    movetime = 10   # in ticks

    # Modifiers and bonuses
    ACbonus = 0        # Ac bonus added in Entity.set_derived_stats
    arms = 2           # number of arms available for arming
    heal_mult = 1      # multiplier on healing items
    poison_mult = 1    # multiplier on poison damage
    radiation_mult = 1 # multiplier on radiation damage
    raim = 0           # bonus to ranged to-hit
    maim = 0           # bonus to melee to-hit
    mdmg = 0           # bonus to melee damage


    # player-visible regular stats
    statnames = ("Str", "Dex", "Con", "Psy", "AC")
    statattrs = ("str", "dex", "con", "psy", "AC")

    # all Stat attributes
    all_statattrs = ("hp", "maxhp", "str", "dex", "con", "psy", "AC", "ACbonus", "exp", 
                     "movetime", "arms", "heal_mult", "poison_mult", "radiation_mult",
                     "raim", "maim", "mdmg", "mp", "maxmp", "stam", "maxstam")

    _derived = None

    def save_stats(self, name):
        saved = Stats()
        for attr in Stats.all_statattrs:
            setattr(saved, attr, getattr(self, attr))
        setattr(self, name, saved)

    def autoupdate_derived_stats(self, derived):
        """Link another Stats object to this one, so that any change to one of self's
        stats causes an equal change in derived's stats, but not the other way around."""
        assert isinstance(derived, Stats)
        self._derived = derived

    def __setattr__(self, attr, value):
        "Implements autoupdate_derived_stats magic"
        if self._derived and hasattr(self, attr):
            oldval = getattr(self, attr)
            setattr(self._derived, attr, getattr(self._derived, attr) + value - oldval)
        object.__setattr__(self, attr, value)

    def speed_percent(self):
        return int(100 * (10.0 / self.movetime))

class Buff(object):
    name = ""

    def __init__(self, towho, length, wearoff): #, *msg_parts):
        #self.message = form_msg(*msg_parts)
        State.turnlist.add(self, length)
        self.towho = towho
        self.wearoff = wearoff
        self.remembered_attrs = []
        towho.buffs.append(self)

    # def add_effect(self, end_msg = None):
    #     self.end_msgs.append(end_msg)

    def remember(self, *attrs):
        for attr in attrs:
            self.remembered_attrs.append((attr, getattr(self.towho, attr)))

    def restore(self):
        for attr, value in self.remembered_attrs:
            setattr(self.towho, attr, value)

    def remove(self):
        State.turnlist.remove(self)
        self.towho.buffs.remove(self)

    def take_turn(self):
        "Time up!"
        self.remove()
        self.wearoff()
        return 0

    def time_to_go(self):
        return State.turnlist.time_to_go(self)

    def extend(self, ticks):
        State.turnlist.add_time(self, ticks)


def poison(who):
    def wearoff():
        if who == State.player:
            console("You feel much better now")
        else:
           console(who, "^looks much better")
        who.poisoned = False
    if who.isgoo:
        console(who, "^pusses blue bubbles")
        who.take_damage(10)
    else:
        if who.poisoned:
            pass
            #console(who, "^looks even worse")
        else:
            buff = Buff(who, 10 * max(2, randint(1,20) - who.con), wearoff)
            buff.name = "poison"
            who.poisoned = True
        if who == State.player:
            console("Your veins burn with poison!")
        else:
            console(who, "'s", "wound festers!")


class GooWorker(object):
    def __init__(self, who):
        who.turning_to_goo = True
        State.turnlist.add(self, 15)
        self.progress = 0
        self.who = who

    def take_turn(self):
        who = self.who
        if who.pos == None:
            #has died
            State.turnlist.remove(self)
            del self.who
            return 0
        if self.progress == 0:
            console(who, "'s", "skin seems to bubble!")
            State.turnlist.add_time(self, 20)
        elif self.progress == 1:
            console(who, "^is definitely turning green...")
            who.hp += 5
            who.maxhp += 5
            State.turnlist.add_time(self, 20)
        elif self.progress == 2:
            console("streams of ooze flow down", who, "'s", "arms")
            who.movetime += 5
        elif self.progress == 3:
            console(who, "^is slowing to a crawl...")
            who.movetime += 15
            who.hp += 15
            who.maxhp += 15
        elif self.progress == 4:
            console(who, "^has turned into a mound of goo!", colour = Color(128,255,128))
            self.become_goo()
            State.turnlist.remove(self)
            return 0
        State.turnlist.add_time(self, who.movetime)
        self.progress += 1
        return 0

    def become_goo(self):
        who = self.who
        who.turning_to_goo = False
        who.isgoo = True
        if who == State.player:
            console("Your thoughts cease... silence...", colour = Color('Blue'))
        who.psy = 0
        who.dex -= 3
        who.con += 10
        who.str += 5
        who.hp += 30
        who.maxhp += 30
        who.descr = "A senient mound of goo"
        who.hand_name = "gooey appendage"
        who.hand_type = Entity.NO_FINGERS
        who.arms = 3
        if hasattr(who, "modifier"):
            del who.modifier
        who.set_image("goo1")
        if len(who.inventory):
            console(who, "^absorbs everything", who, "^is carrying")
        temp = who.inventory
        for item in temp:
            if item.category == "Consumables":
                who.consume(item)
                who.inventory.remove(item)

def turning_into_goo(who):
    if who.isgoo:
        return
    if who.turning_to_goo:
        # what, twice?
        console(who, "'s", "desperate act isn't going to work!")
        return
    GooWorker(who)

class RegenTimer(object):
    def __init__(self, regen_len):
        self.regen_len = regen_len
        self.start = State.turnlist.time

    def value(self):
        timeouts = int((State.turnlist.time - self.start) / self.regen_len)
        self.start += timeouts * self.regen_len
        return timeouts

class Entity(Noun, Stats):
    # is deleted if pos == None

    descr = "What is it?"  # description
    name = "ENTiTY"
    poison_melee = 0.00    # chance of inflicting poison when meleeing
    hp_regen_len = 500
    mp_regen_len = 50
    stam_regen_len = 10
    hand_name = "hand"  # claws, tentacles, pinchers
    feet_name = "feet"  # we don't use this... but why not

    NO_HANDS = 0
    NO_FINGERS = 1  # can't use ranged weapons
    TENTACLES = 2  # can't use ranged weapons
    POOR_HANDS = 3  # can use weapons
    GOOD_HANDS = 4   # can use weapons without bumbling

    hand_type = GOOD_HANDS

    # Bits
    ismutant = True  # it's the default!
    isenemy = False
    isgoo = False
    turning_to_goo = False
    poisoned = False
    can_count = True  # can count items

    def __init__(self, img_file, flip_image = False):
        self.set_image(img_file, flip_image)
        self.flags = 0
        self.dir = LEFT
        self.inventory = []
        self.pos = None  #Nowhere
        self.oldpos_offset = None  #previous tile, when sliding. for sliding only!
        self.pixelpos = None
        self.unique = False
        #self.modifier describes entities with modified appearance, eg. "three-armed"
        #self.genstats()
        State.turnlist.add(self, uniform(0,1))  #hack
        self.hpregen = RegenTimer(self.hp_regen_len)
        self.mpregen = RegenTimer(self.mp_regen_len)
        self.stamregen = RegenTimer(self.stam_regen_len)
        self.buffs = []
        #self.buffed_attrs = []
        self.mutations = []
        self.defects = []
        self.equipment = {}
        # Each Entity has (at least) 3 sets of stats: 
        # The pre-mutation values, equal to random rolls + racial bonuses, or monster stats,
        # entity.racestats.<stat>,
        # The normal stats, which include any mutation/defect effects, entity.basestats.<stat>
        # And the current values, entity.<stat> which include temporary effects
        self.save_stats("racestats")
        self.save_stats("basestats")
        self.racestats.autoupdate_derived_stats(self.basestats)
        self.basestats.autoupdate_derived_stats(self)

    def set_derived_stats(self):
        """This should be called after mutations, etc, have been applied. It modifies self.basestats
        maxhp, maxstam are derived, maxhp is partially (Con-dependent)"""
        base = self.basestats
        if self.isenemy == False:
            base.maxhp = self.racestats.hp + base.con
        base.AC = self.racestats.AC + (base.dex - self.racestats.dex) + base.ACbonus  #UGH
        base.hp = base.maxhp
        base.mp = base.maxmp = base.psy
        base.stam = base.maxstam = base.dex + base.con

    def set_image(self, img_file, flip_image = False):
        self.img = gfx.sprites[img_file]
        if flip_image:
            self.img2 = gfx.flipped(gfx.sprites, img_file)
        self.flip_image = flip_image
        self.image_offset = self.img.get_rect(bottomright=(40,40))

    def check_visible(self):
        if not self.pos:
            return True  # umm....
        tile = State.map.tile(self.pos)
        return tile.flags & Tile.VISIBLE

    def die(self, cause = ""):
        State.turnlist.remove(self)
        if cause != "":
            cause = " " + cause
        if self == State.player:
            console("You die" + cause, "...", colour = Color('Red'))
            State.gameover = True
        else:
            console(self, "^dies", cause)
            m = State.map
            tile = m.tile(self.pos)
            for it in self.inventory:
                tile.add_item(it)
            self.inventory = []
            self.equipment.clear()
            m.move_entity(self, None)

    def take_turn(self):
        "call each move"
        if self.poisoned:
            self.hp -= 1
            if self.hp <= 0:
                self.die("from poison")

        self.hp = min(self.maxhp, self.hp + self.hpregen.value())
        self.mp = min(self.maxmp, self.mp + self.mpregen.value())
        self.stam = min(self.maxstam, self.stam + self.stamregen.value())

    def take_damage(self, amount):
        console(self, "^takes", str(amount), "damage")
        self.hp -= amount
        if self.hp <= 0:
            self.die()

    def calc_pixelpos(self, slide_frac):
        self.pixelpos = (Vec2d(self.oldpos_offset)*(1.0 - slide_frac) + self.pos)*40
        ##print "entity at", self.pos, "pixelpos=", self.pixelpos, "oldoff = ", self.oldpos_offset, "slide=", slide_frac

    def get_image(self):
        "Visibility of the entity is handled elsewhere"
        if self.dir == RIGHT and self.flip_image:
            return self.img2
        else:
            return self.img

    def get_name(self):
        if self == State.player: return "you"
        ret = "the " + self.name
        if not self.unique and hasattr(self, 'modifier'):
            ret = self.modifier + " " + ret
        return ret

    def fail_msg(self, *args, **kwargs):
        """Returns False. Equivalent to a call to console if self == player, otherwise no message."""
        if self == State.player:
            console(*args, **kwargs)
        return False

    def try_reload(self):
        "Returns time spent (0 if couldn't)"
        weapon = self.equipment.get("weapon")
        if not weapon:
            return self.fail_msg("You're not holding anything!")
        if not hasattr(weapon, 'magazine'):
            return self.fail_msg("Your", weapon, "doesn't take ammunition")
        if weapon.shots >= weapon.magazine:
            return self.fail_msg("Your", weapon, "is already fully loaded")
        # It's assumed that all ammo of the same type is grouped together
        for it in self.inventory:
            if type(it) == weapon.ammo:
                amount = capped_adjust(weapon.shots, (0, weapon.magazine), it.amount)
                weapon.shots += amount
                it.amount -= amount
                if it.amount <= 0:
                    self.inventory.remove(it)
                console(self, "^reloads", self, "'s", weapon) 
                return weapon.reloadtime
        return self.fail_msg("You haven't got any ammo for this gun")

    def try_move(self, wantmove):
        "Returns newpos if move happened."
        newpos = State.map.can_move(self.pos, wantmove)
        if not newpos:
            return False
        if newpos != self.pos:
            State.map.move_entity(self, newpos)
            if self.isgoo:
                tile = State.map.tile(newpos)
                if tile.contents:
                    temp = tile.contents
                    for thing in temp:
                        if thing.category == "Consumables":
                            self.consume(thing)
                            tile.contents.remove(thing)
            return newpos
        return False

    def consume(self, item):
        "Returns time spent. The caller must remove the item from inventory/whatever!"        
        console(self, "^consumes", "a", item)
        item.consume(self)
        return 10

    def _pickup(self, item):
        "add to inventory, combining ammo as needed"
        if hasattr(item, "shots"):
            for it in self.inventory:
                if type(it) == type(item):
                    #combine
                    it.shots += item.shots
                    return
        self.inventory.append(item)

    def hands(self):
        name = self.hand_name
        if name == "hand" and self.hand_type < Entity.GOOD_HANDS:
            # somewhat lazy to special-case this here
            name = "crude hand"
        if self.arms == 0:
            return "teeth"
        elif self.arms == 1:
            return name
        return pluralise(name)

    def set_num_arms(self, arms):
        "Drop stuff as needed."
        # FIXME
        self.basestats.arms = arms

    def try_pickup(self, item):
        "Returns time spent, or None for fail (currently can't fail)"
        tile = State.map.tile(self.pos)
        if not tile.contents:
            return 0
        tile.contents.remove(item)
        State.map.update_itemlayer(1)
        if self.isgoo:
            if item.category == "Consumables":
                return self.consume(item)
            else:
                self._pickup(item)
                console(self, "^envelopes", "the", item)
                item.make_gooey()
            return 10
        else:
            self._pickup(item)
            console(self, "^picks up", "the", item)
            return 10

    def has_equipped(self, item):
        "Returns equip type"
        if item in self.equipment.values():
            return item.equip_as
        return False

    def _equip(self, item):
        assert(self.equipment.get(item.equip_as) == None)
        self.equipment[item.equip_as] = item
        bonuses = getattr(item, "bonuses", {})
        for stat, value in bonuses.iteritems():
            #print "added", value, "bonus to", stat
            setattr(self, stat, getattr(self, stat) + value)

    def _unequip(self, item):
        "Remove item from equipment; returns what it was"
        for k, v in self.equipment.iteritems():
            if v is item:
                self.equipment.pop(k)

                bonuses = getattr(item, "bonuses", {})
                for stat, value in bonuses.iteritems():
                    #print "removed", value, "bonus to", stat
                    setattr(self, stat, max(0, getattr(self, stat) - value))

                return k
        return False

    def try_unequip(self, item):
        "Dual purpose: Call this when removing from inventory!!"
        what = self._unequip(item)
        if what == False:
            return 0
        elif what == "weapon":
            console(self, "^is now unarmed")
            return 5
        else:
            console(self, "^takes off", "a", item)
            #raise Exception("WTF is a " + str(what))

    def try_drop(self, item):
        "Returns time spent. Can't currently fail"
        assert(item in self.inventory)
        self.inventory.remove(item)
        
        State.map.tile(self.pos).add_item(item)
        if self.isgoo:
            console(self, "^expels", "a", item)
            return self.try_unequip(item) + 20
        else:
            console(self, "^drops", "a", item, "on the ground")
            if self.check_visible() and hasattr(item, "drop_snd"):
                sfx.play(item.drop_snd)
            return self.try_unequip(item) + 5

    def can_equip(self, item):
        if self.hand_type > Entity.NO_HANDS:
            return True
        return False

    def try_equip(self, item):
        ticks = 0
        if item.equip_as == None:
            return False
        if self.can_equip(item) == False:
            ##print "can't equip"
            return False
        oldequip = self.equipment.get(item.equip_as)
        if oldequip:
            self._unequip(oldequip)
            ticks += 3
        self._equip(item)
        if item.equip_as == "weapon":
            if self.hand_type >= Entity.GOOD_HANDS:
                if item.ranged:
                    console(self, "^readies", "a", item)
                elif oldequip:
                        console(self, "^switches to wielding", "a", item)
                else:
                    console(self, "^wields", "a", item)          
                ticks += 4
            else:
                console(self, "^grasps", "a", item, "in", self, "'s", self.hands())
                ticks += 7
        else:  #boots, armour
            if self.arms >= 2 and self.hand_type >= Entity.GOOD_HANDS:
                console(self, "^puts on", "a", item)            
                ticks += 15
            else:
                if self.arms < 2:
                    ticks += 20
                ticks += 5 * (Entity.GOOD_HANDS - self.hand_type)
                console(self, "^struggles to put on", "a", item, "with", self, "'s", self.hands())
        return ticks



###########################   RACES  ###############################



class Human(Entity):
    racename = "Pure-Strain HUMAN"
    blurb = "Pure-strain humans have no hereditary mutations. While this means many of the potential benefits are absent, they have a natural inclination towards relic technology."
    bonuses = {}
    msgs = ""
    ismutant = False

    def __init__(self):
        super(Human, self).__init__("human2")

class MutantHuman(Entity):
    racename = "Mutated HUMAN"
    blurb = "Despised by the pure-strain humans, these creatures are the mutated branch of the human strain. While not as intelligent as their pure-strain counterparts, they are more intelligent than the Scalies, Clickies, or Hairies."
    bonuses = {}
    msgs = ""

    def __init__(self):
        super(MutantHuman, self).__init__("human2")

class Hairy(Entity):
    racename = "Hairy (mutated MAMMAL)"
    blurb = "Mutated from mammals, the Hairies have excellent dexterity and constitution. They are constantly hungry, due to their high metabolism."
    bonuses = {'Dex': +1, 'Con': +1, 'Psy': -1}
    msgs = """You smell some meat cooking in the distance
You smell something sweet, and sugary
You are beginning to day dream about your next meal
You suddenly feel a shiver go up your fur
You feel hot"""
    hand_name = "claws"

    def __init__(self):
        super(Hairy, self).__init__("hairy2")

class Scaly(Entity):
    racename = "Scaly (mutated REPTILE)"
    blurb = "Mutated from reptiles, the Scalies have excellent strength. They have a poor understanding of social concepts, and usually have poor respiratory systems."
    bonuses = {'Str': +1, 'Psy': -1}
    msgs = """You taste ozone on the air
You taste metal in the air
You taste sweat in the air
You taste moisture in the air
Your scales feel dry
You feel cold
Your breathing is raspy"""
    hand_name = "claws"

    def __init__(self):
        super(Scaly, self).__init__("scaly1")

class Clicky(Entity):
    racename = "Clicky (mutated INSECT)"
    blurb = "These humanoid insects have very poor social skills, usually living nomadic lives. When needed, they will co-operate with other creatures. They have a highly developed immune system, but usually have crude hands. They are often believed to be too stupid to be treacherous."
    bonuses = {'Dex': -1, 'Con': +3, 'Psy': -2}
    msgs = """You perceive others have been here recently
You perceive unusual chemicals are near
You perceive a meal is near
You perceive something is near
You are neither hot nor cold"""
    hand_name = "tarsal claw"
    feet_name = "tarsomeres"
    hand_type = Entity.POOR_HANDS

    def __init__(self):
        super(Clicky, self).__init__("clicky1")

playerraces = [Human, MutantHuman, Hairy, Scaly, Clicky] 

#class Player(Hairy):
def makeplayer(basecl):

    ## FIXME: ugly

    def roll_stat():
        return sum(sorted([randint(1,6) for i in range(4)])[1:4])
   
    def playerinit(self):
        #super(Player, self).__init__()
        self.name = "The Forsaken"
        self.pronoun = "you"

        stats = self.racestats
        while 1:
            stats.dex = roll_stat()
            stats.str = roll_stat()
            stats.con = roll_stat()
            stats.psy = roll_stat()
            if stats.psy + stats.str + stats.con + stats.dex == 50:
                break

        stats.AC = stats.dex
        stats.maxhp = stats.hp = randint(2,16)  #+ stats.con

    p = basecl()
    playerinit(p)
    for stat,val in basecl.bonuses.iteritems():
        stat = stat.lower()
        setattr(p.racestats, stat, getattr(p.racestats, stat) + val)

    # This will be called again each time mutations are added
    p.set_derived_stats()
    return p

class TurnList(object):
    def __init__(self):
        self.queue = []
        self.time = 0  #current game clock, in ticks
        
    def add(self, entity, delay = 0):
        entry = [self.time + delay, entity]
        heapq.heappush(self.queue, entry)
        entity.__turnslot = entry

    def remove(self, entity):
        if entity.__turnslot == self.queue[0]:
            heapq.heappop(self.queue)
        else:
            self.queue.remove(entity.__turnslot)
            heapq.heapify(self.queue)
        del entity.__turnslot

    def get_next(self):
        #if len(self.queue):
        self.time = int(self.queue[0][0])
        return self.queue[0][1]

    def add_time(self, entity, ticks):
        "Call after preforming an action taking 'ticks' time"
        if hasattr(entity, "_TurnList__turnslot"):
            entity.__turnslot[0] += ticks
            if entity.__turnslot == self.queue[0]:
                heapq.heapreplace(self.queue, entity.__turnslot)
            else:
                pos = self.queue.index(entity.__turnslot)
                heapq._siftup(self.queue, pos)
        # if not, must have just been removed

    def time_to_go(self, entity):
        "Ticks until turn"
        return entity.__turnslot[0] - self.time
