from entity import *
from random import shuffle
from itertools import chain

class Mutation(object):
    name = ""
    description = ""
    def __init__(self, name, value, **kwargs):
        self.name = name
        self.value = value
        self.description = kwargs.pop("des", "")
        self.excludes = kwargs.pop("exclude", ())
        if not hasattr(self.excludes, '__iter__'):
            self.excludes = (self.excludes,)
        self.bonuses = kwargs

    def full_description(self):
        ret = self.description
        if len(ret): ret += "\n"
        statnames = "str", "dex", "con", "psy", "ACbonus", "mdmg", "maim", "raim"
        fullnames = "str", "dex", "con", "psy", "AC", "melee dmg", "melee aim", "ranged aim"
        names = dict(zip(statnames, fullnames))
        stats = [sign_int(val) + " " + names[k] for k,val in self.bonuses.iteritems() if k in names]
        if "movetime" in self.bonuses:
            # approximate...
            stats.append(sign_int(int(100 * (10.0 / (10 + self.bonuses["movetime"]) - 1.00))) + "% move speed")
        ret += ", ".join(stats)
        if "spell" in self.bonuses:
            ret += "\n<spell descriptor>"
        return ret

    def __str__(self):
        return "<Mutation: " + self.name + ">"

    def apply(self, entity):
        "Add a mutation to an Entity"
        for attr, value in self.bonuses.iteritems():
            if attr in Stats.all_statattrs:
                incattr(entity.basestats, attr, value)
            elif hasattr(entity, attr):
                setattr(entity, attr, value)
            else:
                print "unhandled mutation attr:", attr, "=", value
    
# MUTATIONS

class Blindsense(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Blindsense", 1)
        self.description = "You have an instinctual awareness of monsters in the area"

class KeenEars(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Keen Ears", 1)
        self.description = "Hears monsters from further away"

class KeenEyes(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Keen Eyes", 1)
        self.description = "Spots hidden items more easily."

class OverpoweringStench(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Overpowering Stench", 2)
        self.description = "Monsters in combat get Dex -2 to attacks"

# DEFECTS

class HeightenedMetabolism(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Heightened Metabolism", 1, poison_mult = 1.5)
        self.description = "Needs water after 5 rounds of combat, greater damage from poison"

class PoorBalance(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Poor Balance", 1)
        self.description = "Easily stumble if attacked while moving"

class PoorRespiratory(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Poor Respiratory System", 3)
        self.description = "Fatigued after 5 rounds of combat"

class PainProne(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Prone to Pain", 3)
        self.description = "Temporarily incapacitated by powerful attacks"

class SlipperyHands(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Slime-covered Hands", 4, exclude = "No Arms"),
        self.description = "Chance of weapons slipping out of hands"

class PoorDualBrain(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Poor Dual Brain", 4)
        self.description = "10% chance of confused action when melee attacking"

class CaveEyes(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Cave-adapted Eyes", 5)
        self.description = "Easily Blinded"

class Frenzy(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Frenzy", 5)
        self.description = "10% chance of 'going bonkers' in melee"

class Fits(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Fits", 6)
        self.description = "10% chance per melee round"

class WeaponIncompetent(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Weapon Incompetent", 8)
        self.description = "Never proficient with non-natural weapons"

class Bleeder(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Bleeder", 9)
        self.description = "-1 HP per turn per wound until bleeding stops"

class FearResponse(Mutation):
    def __init__(self):
        Mutation.__init__(self, "Fear Response", 9)
        self.description = "Strong flight instincts cause you to hestitate in combat when things are bad"

mutations = (
    Blindsense(),                         #1
    Mutation("Chameleon Skin",             1, ACbonus = +1),
    Mutation("Double Heal",                1, heal_mult = 2, des = "Regenerate HP at twice normal speed"),
    KeenEars(),                           #1
    KeenEyes(),                           #1
    Mutation("Tail",                       1, dex = +1),
#    Mutation("Tenacles",                   1, str = +1),   # this sounds bad, actually
    Mutation("Claws",                      2, str = +1, hand_name = "claw"),
    Mutation("Dual Brain",                 2, int = +1, psy = +1, defpsy = +2),
    Mutation("Fur",                        2, ACbonus = +2),
    Mutation("Horns",                      2, mdmg = +2),
    OverpoweringStench(),                 #2
    Mutation("Quills",                     2, mdmg = +1),
#   Mutation("Tougher",                    2, lvlup = {'hp': +2}),
    Mutation("Carapace",                   3, ACbonus = +3, dex = -1),
#   Mutation("Haste self",                 3, spell = Haste),   #TODO
    Mutation("Heightened Psy",             3, psy = +5),
#19           Pincers                           2 pincers, 1-6dmg                   3 (extra melee attack)
    Mutation("Scales",                     3, ACbonus = +2),
    Mutation("Fleet Feet",                 4, movetime = -3, exclude = ("Slow", "Real Slow")),
#   Mutation("Psychic Healing",            4, spell = PsyHeal),   #TODO
#   Mutation("Rage",                       4, spell = Rage),   #TODO
    Mutation("Extra Arm",                  5, arms = +1, des = "Can hold an extra weapon, +1 attack"),
    Mutation("Wings",                      5, movetime = -2),
#   Mutation("Brain Bite",                 6, spell = BrainBite),
#   Mutation("Cryogenesis",                6, spell = Cryogenesis),
    Mutation("Exoskeleton",                6, ACbonus = +5),
    Mutation("Heightened Constitution",    6, con = +5),
    Mutation("Heightened Dexterity",       6, dex = +5),
    Mutation("Heightened Strength",        6, str = +5),
    Mutation("Toxic Stinger",              6, poison_melee = 0.10, des = "Chance to inflict poison in melee"),
    Mutation("Lean Nodes",                 7, poison_mult = 0, des = "Immune to poison"),
    Mutation("Extra Arms",                 8, arms = +2),
#   Mutation("Sonic Blast",                8, spell = SonicBlast),
#   Mutation("Spacial Freeze",             9, spell = SpacialFreeze),
    Mutation("Giantic",                    9, str = +5, dex = -2, con = +3, ACbonus = +2),  #aka "Bigger"
    )

defects = (
    Mutation("Distinctive Odour",          1, des = "You smell horrible!", exclude = "Overpowering Stench"),
    Mutation("Weak Emanel",                1, des = "Your teeth are long gone"),

    HeightenedMetabolism(),               #1    ???
    PoorBalance(),                        #1
    Mutation("Reduced Psy",                1, psy = -2, exclude = "Heightened Psy"),
    Mutation("Crude Hands",                1, hand_type = Entity.POOR_HANDS, exclude = "No Arms"),   # see description; FIXME: exclude race clicky
    Mutation("Sensitivity to Poison",      2, poison_mult = 2, des = "Double damage from poison"),
    Mutation("Sensitivity to Radiation",   2, radiation_mult = 2, des = "Double damage from radiation"),
    Mutation("Unsteady Hands",             2, raim = -3, des = "Poor aim with ranged weapons", exclude = "No Arms"),
    PoorRespiratory(),                    #3
    Mutation("Hyperopia",                  3, maim = -3, des = "Poor melee aim due to far-sightedness", exclude = "Keen Eyes"),
    Mutation("Slow",                       3, movetime = +2),
    PainProne(),                          #3,
    PoorDualBrain(),                      #4
    SlipperyHands(),                      #4
    Mutation("Real Slow",                  5, movetime = +3, exclude = "Slow"),
    CaveEyes(),                           #5
    Mutation("Can't Count",                5, can_count = False, des = "You can't keep track of your ammunition"),
    Mutation("Stiff Motion",               5, ACbonus = -3),
    Frenzy(),                             #5
    Fits(),                               #6
    Mutation("Weak Spine",                 6, con = -1, carry = -4, exclude = "Twisted Spine", des = "You find it difficult to carry heavy loads"),
    Mutation("Shortened Arms",             6, str = -2, exclude = "No Arms"),
    Mutation("Reduced Strength",           7, str = -2, exclude = "Heightened Strength"),
    Mutation("Reduced Dexterity",          8, dex = -3, exclude = "Heightened Dexterity"),
    Mutation("Smaller",                    8, con = -1, str = -2),
    WeaponIncompetent(),                  #8
    Bleeder(),                            #9
    FearResponse(),                       #9
    Mutation("Twisted Spine",              9, ACbonus = -1, mdmg = -1, movetime = +1, des = "Your misformed back makes movement and dodging more difficult"),
    Mutation("Joined Fingers",            10, hand_type = Entity.NO_FINGERS, des = "Without fingers, you can't pull a trigger", exclude = "No Arms"),
#   Mutation("Fragile",                   10, des = "Double damage from weapons"),
    Mutation("Reduced Regeneration",      10, heal_mult = 0.5, des = "Half normal healing from restorative items"),
    Mutation("Terrible Vision",           12, raim = -3, maim = -1, sight = -10, des = "Can see creatures out to 10m. Your aim is affected", exclude = ("Hyperopia", "Keen Eyes")),   #FIXME
    Mutation("Reduced Constitution",      15, con = -3, exclude = "Heightened Constitution"),
    Mutation("No Arms",                   20, arms = -2, des = "Good Afternoon, Mr. Stumpy"),
    )


def roll_mutations(mutation_list, prev_mutations = ()):
    """Returns a randomly ordered sub list of mutations from a list, not including
    mutations which exclude each other (including from prev_mutations)"""

    shuffled = list(mutation_list)
    shuffle(shuffled)
    ret = []
    for mut in shuffled:
        conflict_stats = set(mut.bonuses.keys()).intersection(set(["poison_mult", "heal_mult", "arms"]))
        try:
            for picked in chain(prev_mutations, ret):
                if picked.name in mut.excludes or mut.name in picked.excludes:
                    raise StopIteration
                if len(set(picked.bonuses.keys()).intersection(conflict_stats)):
                    raise StopIteration
            ret.append(mut)
        except StopIteration:
            pass
    return ret
