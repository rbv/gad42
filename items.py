import copy
from random import randint, uniform
import pygame
from pygame.locals import *
from text import *
from entity import *
from misc import *
import gfx
from gummworld2 import State, Vec2d



class Item(Noun):
    GOOEY = 1
    IDENTIFIED = 2
    CANT_IDENTIFY = 4

    category = "Misc"
    equip_as = None
    drop_snd = "thump1" #drop1"

    def __init__(self, name, img_file, description, unided = None):
        self.name = name
        self.flags = 0
        self.img_file = img_file
        self.description = description
        #self.img = gfx.items[img_file]
        self.img = gfx.items.get(img_file)
        if not self.img:
            self.img = gfx.MissingImageFile(img_file)
        self.img_dark = gfx.items.get(img_file + "_dark")
        if unided == None:
            self.flags |= Item.IDENTIFIED
            self.unided = None
        else:
            self.unided = unided()

        #self.baseobj = self

    def get_name(self):
        ret = self.name
        if not self.unique and hasattr(self, 'modifier'):
            ret = self.modifier + " " + ret
        if self.unided and self.flags & Item.IDENTIFIED == 0:
            ret = self.unided.name
        return ret

    def get_description(self):
        ret = self.description
        if self.unided and self.flags & Item.IDENTIFIED == 0:
            ret = self.unided.description
        return ret

    def add_modifier(self, text):
        if hasattr(self, "modifier"):
            if text not in self.modifier:
                self.modifier += text + " " + self.modifier
        else:
            self.modifier = text

    def get_image(self, darken = False):
        if darken:
            return self.img_dark
        return self.img

    def new(self):
        #SHALLOW
        return copy.copy(self)

    def make_gooey(self):
        self.add_modifier("gooey")
        self.flags |= Item.GOOEY

def gen_item(itemclass):
    if hasattr(itemclass, "randomly"):
        return gen_one_of(itemclass.randomly)
    else:
        return itemclass()

class Weapon(Item):
    equip_as = "weapon"
    category = "Weapons"
#    drop_snd = "drop2"
    ranged = True
    verb = "^whacks"
    str = 10  #default
    #ammo = None
    #magazine = 0  # size of magazine
    #shots = 0     # amount of ammo loaded == number of shots (hmmm...)
    #reloadtime = 10

    def __init__(self, *args, **kwargs):
        Item.__init__(self, *args, **kwargs)
        if hasattr(self, 'magazine'):
            self.shots = randint(0, self.magazine)

class Boots(Item):
    equip_as = "boots"
    category = "Equipment"
    always_plural = True

class Armour(Item):
    equip_as = "armour"
    category = "Equipment"

class Ammo(Item):
    category = "Ammo"
    always_plural = True

class Consumable(Item):
    category = "Consumables"

    def consume(self, bywho):
        "PLACEHOLDER"
        #console(bywho, "^spits", self, "out!")
        console(bywho, "^spits it out!")


categories = "Weapons", "Equipment", "Consumables", "Ammo", "Misc"
category_types = Item, Weapon, Boots, Armour, Ammo, Consumable


#Ammo----------------------------------------------------

class RegAmmo(Ammo):
    amount = 5
    def __init__(self):
        Item.__init__(self, "regular ammunition", "ammobox1", "generic ammunition which fits most of the weapons found here.")

class EAmmo(Ammo):
    amount = 2
    def __init__(self):
        Item.__init__(self, "energy cell", "ecell", "An amazing piece of technology, which stores incredible amounts of energy. For all models of laser weaponry.")

class BAmmo(Ammo):
    amount = 2
    def __init__(self):
        Item.__init__(self, "Fission Orb", "bcell", "This orb contains super-heated plasma - though it is cool to the touch. For all Blaster weaponry.")



# basic weapons------------------------------------

class Rifle(Weapon):
    ammo = RegAmmo
    magazine = 6
    reloadtime = 15
    speed = 15
    str = 6
    shootsnd = "Crush8-Bit"
    def __init__(self):
        Weapon.__init__(self, "rifle", "rifle2", "It's a rifle.", UnknownRifle)

class Dagger(Weapon):
    speed = 5
    ranged = False
    verb = "^slashes"
    str = 4
    def __init__(self):
        Weapon.__init__(self, "dagger", "dagger1", "A basic dagger: crafted from scrap parts, and sharpened along one side.")

class Pistol(Weapon):
    ammo = RegAmmo
    magazine = 4
    reloadtime = 15
    speed = 5
    str = 4
    shootsnd = "thump2"
    def __init__(self):
        Weapon.__init__(self, "pistol", "pistol", "A small pistol.", UnknownPistol)

class Fpistol(Weapon):
    ammo = RegAmmo
    magazine = 1
    reloadtime = 10
    speed = 10
    str = 5
    shootsnd = "thump2"
    def __init__(self):
        Weapon.__init__(self, "flintlock pistol", "pistol", "A flintlock pistol. This is fired with gunpower. Unreliable, slow and weak.", UnknownPistol)

class Club(Weapon):
    ranged = False
    speed = 8
    str = 4
    def __init__(self):
        Weapon.__init__(self, "club", "club1", "A broken tree branch. Good for giving someone a whack with.")


# advanced weapons---------------------------------

class Bpistol1(Weapon):
    ammo = BAmmo
    magazine = 2
    reloadtime = 10
    speed = 5
    str = 10
    shoottext = "PZZZZZT!"
    shootsnd = "KirbyStyleLaser"
    def __init__(self):
        Weapon.__init__(self, "Blaster Mk.II", "pistol", "The TE02 blaster. This weapon fires energy and uses radioactive cells for its ammunition.", UnknownPistol)

class Brifle1(Weapon):
    ammo = BAmmo
    magazine = 2
    reloadtime = 10
    speed = 15
    str = 20
    shoottext = "SZZZZZT!"
    shootsnd = "KirbyStyleLaser"
    def __init__(self):
        Weapon.__init__(self, "Blaster Mk.IV", "rifle2", "The TE04 blaster. This weapon fires concentrated bursts of energy.",UnknownRifle)

class Lpistol1(Weapon):
    ammo = EAmmo
    magazine = 2
    reloadtime = 10
    speed = 10
    str = 12
    shoottext = "ZZZZP!"
    shootsnd = "KirbyStyleLaser"
    def __init__(self):
        Weapon.__init__(self, "Laser Lv-02", "pistol", "This weapon fires super-high frequency light. It's shaped like a pistol.",UnknownPistol)

class Lrifle1(Weapon):
    ammo = EAmmo
    magazine = 2
    reloadtime = 10
    speed = 20
    str = 25
    shoottext = "ZZZZZT!"
    shootsnd = "KirbyStyleLaser"
    def __init__(self):
        Weapon.__init__(self, "Laser LMLv-88", "rifle2", "Anti-armour, heavy duty laser.",UnknownRifle)






# consumable items---------------------------------

class FoodPill(Consumable):
    always_plural = True
    def __init__(self):
        Item.__init__(self, "food pills", "fpill2", "An assortment of large pills. This supplement has everything needed to sustain oneself for a full day.", UnknownPill)

    def consume(self, bywho):
        if bywho.isgoo:
            bywho.hp = min(bywho.maxhp, bywho.hp + 15)
            console(bywho, "^seems to turn a deeper shade of green")
        elif bywho == State.player:
            console("Munch, munch.")

class PoisonPill(Consumable):
    def __init__(self):
        Item.__init__(self, "(mutant) rat poison", "fpill2", "<NOT USED>", UnknownPill)
        self.flags |= Item.CANT_IDENTIFY  # muhahaha!

    def consume(self, bywho):
        poison(bywho)

class StrMed(Consumable):
    def __init__(self):
        Item.__init__(self, "Hyperoid", "str_boost", "This drug enhances strength for a period.", UnknownMed)

    def consume(self, bywho):
        for b in bywho.buffs:
            if b.name == "str":
                r = randint(0,3)
                if not(bywho.isgoo) and r == 0:
                    #GOO!
                    console(bywho, "^convulses -- too much hyperoid!") 
                    State.turnlist.add_time(bywho, 10)
                    turning_into_goo(bywho)
                elif r <= 1:
                    if bywho == State.player:
                        console("You feel stabbing pain in your chest!")
                    else:
                        console(bywho, "^screams in pain!")
                    bywho.take_damage(8)
                else:
                    #extend
                    console(bywho, "'s", "strength is renewed!")
                    b.extend(10 * randint(5, 10))
                return
        #not already strength-doped
        amount = randint(2, 10)
        bywho.str += amount
        def wearoff():
            bywho.str -= amount
            if bywho == State.player:
                console("Your strength returns to normal")
            else:
                console(bywho, "^seems to look less blood-thirsty")
        buff = Buff(bywho, 10 * randint(5,15), wearoff)
        buff.name = "str"    
        if bywho == State.player:
            console("You feel superpowered!")
        else:
            console(bywho, "^looks meaner than ever!")


class HasteMed(Consumable):
    def __init__(self):
        Item.__init__(self, "Hyperhaler", "haste_boost", "An inhaler which increases speed for a period.", UnknownMed)
        self.adjust_amount = 0

    def consume(self, bywho):
        for b in bywho.buffs:
            if b.name == "speed":
                adjust = capped_adjust(bywho.movetime, (3,99), -2)
                if adjust == 0:
                    console(bywho, "can't possibly move faster!")
                else:
                    console(bywho, "^speeds up even more!")
                    self.adjust_amount += adjust
                return
        #not already speed-doped
        self.adjust_amount = capped_adjust(bywho.movetime, (3,99), -int(bywho.movetime / 2))
        bywho.movetime += self.adjust_amount
        def wearoff():
            bywho.movetime -= self.adjust_amount
            console(bywho, "^slows down")
        buff = Buff(bywho, 10 * randint(5,15), wearoff)
        buff.name = "speed"
        if bywho == State.player:
            console("You feel a rush!")
        else:
            console(bywho, "^speeds up!")

class CoF(Consumable):
    def __init__(self):
        Item.__init__(self, "Can of Flesh", "cof", "A can of nutrients that promotes the rapid regeneration of skin and flesh.")

    def consume(self, bywho):
        if bywho.hp < bywho.maxhp:
            console(bywho, "'s", "wounds heal in seconds!")
            bywho.hp = min(bywho.hp + 15, bywho.maxhp)
        else:
            console(bywho, "^burps flesh")

class Tentacle(Consumable):
    def __init__(self):
        Item.__init__(self, "severed tentacle", "tentacle", "The severed tentacle of a land octopus. Known to have regenerative powers, and a local delicacy.")

    def consume(self, bywho):
        if bywho.hp < bywho.maxhp:
            console(bywho, "^licks", bywho, "'s", "lips")
            bywho.hp = min(bywho.hp + 5, bywho.maxhp)
        else:
            console("Tasty!", entity = bywho)

class Antidote(Consumable):
    def __init__(self):
        Item.__init__(self, "antidote", "antidote", "Miracle drug, cures most venoms and poisons.")

    def consume(self, bywho):
        for b in bywho.buffs:
            if b.name == "poison":
                console(bywho, "^feels much better!")
                b.remove()
                return
        console("No effect", entity = bywho)

#   Equipment


class RegBoots(Boots):
    bonuses = {'AC': 1}
    def __init__(self):
        Item.__init__(self, "worn boots", "boots1", "An old, but sturdy pair of boots.")


class GoodBoots(Boots):
    bonuses = {'AC': 2}
    def __init__(self):
        Item.__init__(self, "military boots", "boots2", "Military issue, and apparently unworn too.")

class BodyArmour(Armour):
    bonuses = {'AC': 6}
    def __init__(self):
        Item.__init__(self, "body armour", "body_armour1", "Military issue body armour. Extremely tough.")


# unidentified items------------------------------------


class UnknownPill(Consumable):
    randomly = (FoodPill, 0.7, PoisonPill, 0.3)
    def __init__(self):
        Item.__init__(self, "pill", "fpill2", "An unknown pill. Could be anything.")

class UnknownPistol(Weapon):
    randomly = (Pistol, 0.3, Fpistol, 0.2, Bpistol1, 0.3, Lpistol1, 0.2)
    def __init__(self):
        Weapon.__init__(self, "pistol", "pistol", "Some kind of gun.") # It's heavy.")

class UnknownRifle(Weapon):
    randomly = (Rifle, 0.5, Brifle1, 0.3, Lrifle1, 0.2)
    def __init__(self):
        Weapon.__init__(self, "rifle", "rifle2", "Some kind of rifle.") # It's heavy.")

class UnknownMed(Consumable):
    randomly = (StrMed, 0.5, HasteMed, 0.5)
    def __init__(self):
        Item.__init__(self, "drug", "str_boost", "It's a drug. Who knows what it does?")


all_items = [a for a in globals().values()
             if isinstance(a, type) and Item in a.__mro__ and a not in category_types and not hasattr(a, 'randomly')]
