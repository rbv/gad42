## SDL/pygame specific graphics code

import pygame
from pygame import PixelArray, Color
from pygame.locals import *
import os
import copy

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

sprites = {}
tiles = {}
items = {}

class Screen():
    @staticmethod
    def init(size, caption):
        Screen.surface = pygame.display.set_mode(size, 0)
        pygame.display.set_caption(caption)
        Screen.rect = Screen.surface.get_rect()

    @staticmethod
    def blit(surf, pos, area = None, flags = 0):
        Screen.surface.blit(surf, pos, area, flags)

    @staticmethod
    def flip():
        pygame.display.flip()

    @staticmethod
    def fill(colour = (0,0,0)):
        Screen.surface.fill(colour)


class MissingImageFile(object):
    content = ""
    def __init__(self,file):
        self.content = file
        print "missing file: " + file
    def __str__(self):
        return "missing file: " + content

def darken_image1(surface):
    surf2 = surface.copy()
    parray = PixelArray(surf2)
    for row in xrange(len(parray)):
        for col in xrange(len(parray[0])):
            r,g,b = surf2.unmap_rgb(parray[row,col])
        #            pixel = c.correct_gamma(0.5)
            parray[row,col] = (c.r / 2, c.g / 2, c.b / 2)
#    parray[:] = (0,0,0)
    del parray
    return surf2


def darken_image2(surface, value):
    "Value is 0 to 255, where 0 is black, 255 is no darkening."
    dark = pygame.Surface(surface.get_size(), 24)
    dark.set_alpha(255 - value, pygame.RLEACCEL)
    if surface.get_colorkey():
        surf2 = surface.convert_alpha()
    else:
        surf2 = surface.copy()
    #broken: blends together transparent bits
    surf2.blit(dark, (0, 0), None)
    return surf2

def darken_image(surface, value):
    "Value is 0 to 255, where 0 is black, 255 is no darkening."
    if surface.get_colorkey():
        surf2 = surface.convert_alpha()
    else:
        surf2 = surface.copy()
    surf2.fill(pygame.Color(value,value,value), None, BLEND_RGB_MULT)
    return surf2


def load(container, directory, colorkey=False):
    for f in os.listdir(directory):
        splitname = os.path.splitext(f)
        if splitname[1] != ".bmp":
            print "skipping load of", f
            continue
        img = pygame.image.load(os.path.join(directory, f))
        #remove alpha layer
        if colorkey:
            img = img.convert(24)
            #colorkey = img.get_at((0, 0))
            colorkey = Color(255, 0, 255)
            img.set_colorkey(colorkey)
        #if colorkey:
        #colorkey = pygame.Color(255, 0, 255, 255)
        container[splitname[0]] = img
#        print splitname[0], 

def darkened_gfx(container, dest_container, suffix="_dark", amount=70):
    new = {}
    for name, img in container.iteritems():
        new[name + suffix] = darken_image(img, amount)
    dest_container.update(new)

def flipped(container, name):
    img = container.get(img_file + "_flip")
    if img:
        return img
    flippedimg = img #FIXME
    container[img_file + "_flip"] = flippedimg
    return flippedimg

def darken_colour(colour, mult=0.70):
    col2 = copy.copy(colour)
    col2.r = int(col2.r * mult)
    col2.g = int(col2.g * mult)
    col2.b = int(col2.b * mult)
    return col2

def draw_textbox(rect, *colorargs):
    textbox = pygame.Surface((rect.width, rect.height))
    textbox.fill(pygame.Color('black'))
    textbox.set_alpha(200)
    Screen.blit(textbox, rect)
    pygame.draw.rect(Screen.surface, pygame.Color(*colorargs), rect, 3)

