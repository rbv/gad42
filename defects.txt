Random defects:


number               Name                    Summary                 value
1                    Distinctive Ordor       Smell horrible!         1
2                    Reduced Psychic         Psy -2                  1
3                    Slow                    Move slower             1
4                    Stiff Motion            can't run               3
5                    Terrible Reflex         Dex -3 when attacked    4
6                    Poor Dual Brain         small chance of hitting 4
                                             yourself when in melee   
7                    Frenzy                  10% chance of           5
                                             'going bonkers' in melee 
8                    Fits                    10% chance of being      6
                                             unable to attack in melee
9                    Reduced Strength        Str -2                   7
10                   Weapon Incompetent      Dex -2 when attacking    8
                                             with weapons       
11                   Reduced Dex             Dex -3                   8
12                   Bleeder                 +1 extra dmg when hit    9
13                   Fragile                 suffers double dmg       10
14                   Reduced Constitution    Con -3                   15
15                   No Arms                 Good afternoon, Mr.Stumpy 25
