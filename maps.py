
import paths
import random
import pygame
from pygame.locals import *
from text import *
from misc import *
import ppfov
import gfx
from gfx import Screen

from gummworld2 import State, Vec2d

#tileid -> Surface
tile_images = []
tile_dark_images = []
#variant descriptors are duplicated
#tileid -> TileType
tileinfo = []

class TileType(object):
    def __init__(self, basetile, name, flags):
        self.name = name
        self.flags = flags
        self.basetile = basetile
        #self.variants: list of (accum, tileid) pairs
        
    def __repr__(self):
        rep = "TileType(%d, %s, %s) {" % (self.basetile, self.name, hex(self.flags))
        for accum, variant in self.variants:
            rep += "%.2f=%d " % (accum, variant)
        return rep + "}"

class Tile(object):
    VISIBLE = 0x1
    SEEN = 0x2
    BLOCKSIGHT = 0x4
    OBSTRUCTS = 0x8
    ISWALL = 0x10

    def __init__(self, tileid=None):
        self.flags = 0
        self.tileid = None
        if tileid is not None:
            self.set_tileid(tileid)
        self.contents = None
        self.entity = None
        self.z = 99

    def set_tileid(self, t):
        self.tileid = t
        #self.basetile = tile_desciptors[t].basetile
        self.flags = tileinfo[t].flags

    def add_item(self, item):
        "Adds an item to this Tile's contents, making sure the draw order is correct"
        if not self.contents:
            self.contents = [item]
        else:
            #FIXME
            self.contents.append(item)

    def is_free(self):
        "Nothing already on this tile"
        if self.flags & (Tile.ISWALL | Tile.OBSTRUCTS) == 0:
            if self.contents == None and self.entity == None:
                return True
        return False
    
    def get_image(self):
        if self.flags & Tile.VISIBLE:
            return tile_images[self.tileid]
        elif self.flags & Tile.SEEN:
            return tile_dark_images[self.tileid]
        else:
            return gfx.tiles["black"]

    def get_item_image(self):
        if self.flags & Tile.SEEN == 0:
            return None
        elif self.contents:
            return self.contents[0].get_image(self.flags & Tile.VISIBLE == 0)
        else:
            return None


WALLBITS = Tile.ISWALL | Tile.OBSTRUCTS | Tile.BLOCKSIGHT

#organised into variant groups
_tile_data = (
    {'name':"floor1", 'flags':0, 'variants':(
#            GRAPHIC,  WEIGHTING
            ("tile1", 70),
            ("tile1var1", 3),
            ("tile1var2", 3),
            ("tile1var3", 3),
            ("tile1var4", 1),
            ("tile1var5", 1),
            ("tile1var6", 1),
            ("tile1var7", 1),
            ("tile1var8", 1),
        )
     },

    {'name':"floor2", 'flags':0, 'variants':(
            ("tile2", 30),
            ("tile2var1", 1),
            ("tile2var2", 1),
            ("tile2var3", 0.3),
        )
     },

    {'name':"floor3", 'flags':0, 'variants':(
            ("tile3", 30),
            ("tile3var1", 1),
            ("tile3var2", 1),
            ("tile3var4", 1),
            ("tile3var5", 1),
        )
     },


    {'name':"swall", 'flags':WALLBITS, 'img':"swall3"},
    
    {'name':"swall_f", 'flags':WALLBITS, 'variants':(
            ("ffwall1", 10),
            ("ffwall2", 1),
            ("ffwall4", 1),
            )
     },

    #A unique tile
    {'name':"burningcar", 'img':"streetrubble1", 'flags':WALLBITS},
)

def append_tile_gfx(img_name):
    try:
#        print "appending " + str(len(tile_images)) + ": " + img_name
        tile_images.append(gfx.tiles[img_name])
        tile_dark_images.append(gfx.tiles[img_name + "_dark"])
    except KeyError as k:
        print
        print "ERROR:"
        print "Graphics file " + str(k) + ".bmp was missing from Graphics/tiles directory!"
        exit()

def init_tiles():
    global tileinfo
    for desc in _tile_data:
        copies = 1
        name = desc.get('name', desc.get('img'))
        tilenum = len(tileinfo)
        tiletype = TileType(tilenum, name, desc.get('flags', 0))
        globals()["T_" + name.upper()] = tilenum  #tiletype
        if desc.has_key('img'):
            append_tile_gfx(desc['img'])
        if desc.has_key('variants'):
            copies = len(desc['variants'])
            totalw = float(sum(var[1] for var in desc['variants']))
            accum = 0.0
            tiletype.variants = []
            for var in desc['variants']:
                accum += var[1] / totalw
                tiletype.variants.append( (accum, tilenum) )
                tilenum += 1
                append_tile_gfx(var[0])
            if accum < 0.999 or accum > 1.001:
                raise Exception("ERROR: sum = %f" % accum)
        else:
            tiletype.variants = [(1, tilenum), ]
            
        tileinfo += [tiletype] * copies

def debug_tileinfo():
    print "tileinfo: "
    for i in range(len(tileinfo)):
        if i == 0 or tileinfo[i] is not tileinfo[i - 1]:
            print "    [%d]: " % i + repr(tileinfo[i])
        else:
            print "    [%d]: DUP" % i

class Camera(object):
    "Damnit GW2, I give up on you. Camera is unbelievably impossible to use, and broken"

    def __init__(self):
        self.rect = Screen.rect.copy()
        self.visible_tile_range = []

    def screen_to_world(self, xy):
        return Vec2d(xy + self.rect.topleft)

    def world_to_screen(self, xy):
        return Vec2d(xy - self.rect.topleft)

    def screen_to_tile(self, xy):
        ##print "screenpos", self.rect.topleft, "mousepos", xy
        worldpos = (Vec2d(xy) + Vec2d(self.rect.topleft)) / 40
        return worldpos

    def setcentre(self, pos):
        mw, mh = map = State.map.width, State.map.height
        tile_x,tile_y = 40, 40
        l,t = pos - Screen.rect.center
        self.rect.topleft = l,t
        self.position = Vec2d(l,t)
        w,h = self.rect.size
        r = l+w-1
        b = t+h-1
        left = int(float(l) / tile_x)
        right = int(float(r + tile_x - 1) / tile_x)
        top = int(float(t) / tile_y)
        bottom = int(float(b + tile_y - 1) / tile_y)
        left = max(0, left)
        top = max(0, top)
        right = min(mw - 1, right) + 1  # + 1 for range's right arg
        bottom = min(mh - 1, bottom) + 1
        ##print l, t, r, b
        ##print left, top, right, bottom
        self.visible_tile_range[:] = [left,top,right,bottom]

class TileLayer(list):
    "A 2D array of surfaces"

    def __init__(self, tile_size, map_size, visible=True):
        self.tile_size = tile_size
        self.map_size = map_size
        self.visible = visible

    def draw(self):
        """Draw visible tiles.

        This function assumes that the tiles stored in the map are surfaces.
        """
        camera = State.camera
        blit = Screen.surface.blit
        cx,cy = camera.rect.topleft
        if not self.visible:
            return
        left,top,right,bottom = camera.visible_tile_range
        mapw,maph = self.map_size
        tw,th = self.tile_size
        if left < 0: left = 0
        if top < 0: top = 0
        if right >= mapw: right = mapw #- 1
        if bottom >= maph: bottom = maph #- 1
        for y in range(top,bottom):
            yoff = y * mapw
            start = yoff + left
            end = yoff + right
            absx = left * tw
            absy = y * th
            for i,s in enumerate(self[start:end]):
            #for s in self[start:end]:
                if s:
                    try:
                        blit(s, (absx-cx, absy-cy))
                    except Exception as e:
                        print left+i, y, str(e)
                        raise
                    #     print s
                    #     print i + start, y
                    #     print s.image
                    #     print e                        
                absx += tw


class Map(object):
    def __init__(self, width, height):
        self.tiles = []
        for i in xrange(width * height):
            self.tiles.append(Tile())
        self.width = width
        self.height = height
        self.entities = []
        self.layers = []  #TileLayers

    def indexOf(self, x, y):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            raise IndexError("Map %dx%d" % (self.width, self.height), x, y)
        return x + self.width * y

    def tile(self, x_or_pair, y = None):
        if y == None:
            y = x_or_pair[1]
            x = x_or_pair[0]
        else:
            x = x_or_pair
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            raise IndexError("Map %dx%d" % (self.width, self.height), x, y)
        return self.tiles[x + self.width * y]
        #return self.tiles[indexOf(x, y)]

    def clip_pos(self, x, y):
        return min(max(x, 0), self.width - 1), min(max(y, 0), self.height - 1)

    def block(self, x1, y1, x2, y2):
        "An iterator (generator) over a rectangle of tiles"
        x1, y1 = self.clip_pos(x1, y1)
        x2, y2 = self.clip_pos(x2, y2)
        for y in range(y1, y2):
            idx = x1 + self.width * y
            for x in range(x1, x2):
                yield x, y, self.tiles[idx]
                idx += 1

    def find_free_tile(self):
        while 1:
            x = random.randint(0, self.width - 1)
            y = random.randint(0, self.height - 1)
            tile = self.tile(x, y)
            if tile.is_free():
                return x, y, tile

    def __set_visible(self, x, y):
        self.tiles[x + self.width * y].flags |= Tile.VISIBLE | Tile.SEEN

    def __check_blocks(self, x, y):
        ##print "BLOCK %d %d = %d  from %d" % (x, y, ret, self.tiles[x + self.width * y].flags)
        return self.tiles[x + self.width * y].flags & Tile.BLOCKSIGHT

    def update_fov(self, player_pos, radius):
        for tile in self.tiles:
            tile.flags &= ~Tile.VISIBLE
        ppfov.fieldOfView(player_pos[0], player_pos[1], self.width, self.height,
                          radius, self.__set_visible, self.__check_blocks)

    def set_tileid(self, x, y, t):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            raise IndexError("%d, %d in Map %dx%d" % (x, y, self.width, self.height))
        self.tiles[x + self.width * y].set_tileid(t)

    def move_entity(self, who, where):
        "Move an entity to a new tile; if where==None, remove from map"
        if who.pos:  #pos == None valid, for initial placement
            assert(self.tile(*who.pos).entity == who)
            self.tile(*who.pos).entity = None
        if where:
            if not isinstance(where, Vec2d):
                where = Vec2d(where)
            assert(self.tile(*where).entity == None)
            self.tile(*where).entity = who
            if who.pos:
                who.oldpos_offset = who.pos - where
                ##print who.oldpos_offset
            else:
                who.oldpos_offset = Vec2d(0, 0)
                self.entities.append(who)
            who.pos = where
            #OH DEAR GOD
            State.app.were_movements = True
        else:
            who.pos = None
            self.entities.remove(who)

    def clean_slides(self):
        for entity in self.entities:
            entity.oldpos_offset = Vec2d(0, 0)

    def create_empty_maplayer(self):
        tw,th = 40, 40
        ml = TileLayer((tw, th), (self.width, self.height)) 
        ml += [None] * (self.height * self.width)
        return ml

    def visible_rect(self):
        left, top, right, bottom = State.camera.visible_tile_range
        left = max(0, left - 1)
        top = max(0, top - 1)
        right = min(self.width - 1, right - 1 + 1) + 1  # + 1 for range's right arg
        bottom = min(self.height - 1, bottom - 1 + 1) + 1
        return left, top, right, bottom

    def can_move(self, pos, wantmove):
        "Check walls and entities in the way. Return newpos if possible"
        if wantmove[0] and wantmove[1]:
            # Prevent moving diagonally around corners
            if self.tile(pos + (wantmove[0], 0)).flags & Tile.ISWALL:
                return False
            if self.tile(pos + (0, wantmove[1])).flags & Tile.ISWALL:
                return False
        wantx, wanty = pos + (wantmove[0], wantmove[1])
        if wantx < 0 or wanty < 0 or wantx >= self.width or wanty >= self.height:
            return False
        desttile = self.tile(wantx, wanty)
        if desttile.flags & Tile.OBSTRUCTS:
            return False
        if desttile.entity:
            return False
        return Vec2d(wantx, wanty)

    def update_distmap(self):
        w, h = self.width, self.height
        tiles = self.tiles
        px, py = State.player.pos

        y1 = max(0, py - 8)
        y2 = min(h - 1, py + 8) + 1
        x1 = max(0, px - 8)
        x2 = min(w - 1, px + 8) + 1
        for y in range(y1, y2):
            idx = x1 + h * y
            for x in range(x1, x2):
                tiles[idx].z = 999999
                idx += 1

        def visit(pos, z):
            if tiles[pos].flags & 0x8: return   #Tile.OBSTRUCTS == 0x8
            if z >= tiles[pos].z: return
            tiles[pos].z = z
            if z >= 70: return
            z += 10
            # blocked is a 9-bit bitfield of directions, numpad ordering:
            # top left is bit 8, bottom right is 0  (bit 4 not used)
            blocked = 0
            # Tile.ISWALL == 0x10
            if tiles[pos - w].flags & 0x10: blocked |= 0b111000000
            if tiles[pos + w].flags & 0x10: blocked |= 0b000000111
            if tiles[pos - 1].flags & 0x10: blocked |= 0b100100100
            if tiles[pos + 1].flags & 0x10: blocked |= 0b001001001

            if not blocked & 0b100000000: visit(pos - w - 1, z + 4)
            if not blocked & 0b010000000: visit(pos - w, z)
            if not blocked & 0b001000000: visit(pos - w + 1, z + 4)
            if not blocked & 0b000100000: visit(pos - 1, z)
            if not blocked & 0b000001000: visit(pos + 1, z)
            if not blocked & 0b000000100: visit(pos + w - 1, z + 4)
            if not blocked & 0b000000010: visit(pos + w, z)
            if not blocked & 0b000000001: visit(pos + w + 1, z + 4)

        visit(self.indexOf(px, py), 0)

    def _hill_climb(self, pos, mult = 1):
        """Returns movediff. mult may be -1 to run away from instead of towards the target.
        Assumptions: pos is not on the map edge, and the map edge is not reachable"""
        bestz = self.tile(pos).z
        ret = None
        for diff in dirdiffs:
            try:
                if self.can_move(pos, diff):
                    thisz = self.tile(pos + diff).z
                    if mult*thisz < mult*bestz:
                        ret = diff
                        bestz = thisz
            except IndexError:
                pass
        return ret

    def pathfind_step(self, from_pos, to_pos, negate = False):
        "Returns diff for a single step"
        mult = -1 if negate else 1
        stepdiff = self._hill_climb(from_pos, mult)
        if stepdiff:
            return stepdiff

        #fallback to crap (FIXME)
        dx, dy = to_pos - from_pos
        if abs(dx) > abs(dy):
            stepdiff = (dx/abs(dx), 0)
        else:
            stepdiff = (0, dy/abs(dy))
        return Vec2d(stepdiff) * mult

    def update_tilelayers(self):
        # for x,y in State.camera.visible_range:
        #     idx = x + self.width * y
        #     maplayer[idx].image = self.tiles[idx].get_image()

        maplayer = self.layers[0]
        x1,y1,x2,y2 = self.visible_rect()
        ##print "UPDATING: ",x1,y1,x2,y2
        for y in range(y1, y2):
            idx = x1 + self.width * y
            for x in range(x1, x2):
                maplayer[idx] = self.tiles[idx].get_image()
                idx += 1

    def update_itemlayer(self, layeridx):
        x1,y1,x2,y2 = self.visible_rect()
        maplayer = self.layers[layeridx]
        for y in range(y1, y2):
            idx = x1 + self.width * y
            for x in range(x1, x2):
                maplayer[idx] = self.tiles[idx].get_item_image()
                idx += 1

    def draw_entitylayer1(self):
        x1,y1,x2,y2 = self.visible_rect()
        screen = Screen.surface.blit
        cx,cy = State.camera.rect.topleft
        tw, th = 40, 40
        for y in xrange(y1, y2):
            start = x1 + self.width * y
            end = start + x2 - x1
            absx = x1 * tw
            absy = y * th
            for tile in self.tiles[start:end]:
                if tile.entity:
                    if tile.flags & Tile.VISIBLE:
                        screen(tile.entity.get_image(), tile.entity.image_offset.move(absx-cx, absy-cy)) # add together
                absx += tw


    def draw_entitylayer(self, slide_frac = 1.0):
        x1,y1,x2,y2 = self.visible_rect()
        screen = Screen.surface.blit
        cx,cy = State.camera.rect.topleft
        tw, th = 40, 40
        entities = []
        for y in xrange(y1, y2):
            start = x1 + self.width * y
            end = start + x2 - x1
            absx = x1 * tw
            absy = y * th
            for tile in self.tiles[start:end]:
                if tile.entity:
                    if tile.flags & Tile.VISIBLE:
                        #screen(tile.entity.get_image(), tile.entity.image_offset.move(absx-cx, absy-cy)) # add together
                        ##print "a", Vec2d(tile.entity.oldpos_offset)
                        ##print "b", Vec2d(tile.entity.oldpos_offset)*40
                        ##print "c", Vec2d(tile.entity.oldpos_offset) *40*(1.0 - slide_frac)
                        tile.entity.calc_pixelpos(slide_frac)
                        entities.append(tile.entity)
                absx += tw
        def entity_y(ent):
            return -ent.pixelpos.y
        entities.sort(key=entity_y)
        for entity in entities:
            screen(entity.get_image(), entity.image_offset.move(entity.pixelpos.x-cx, entity.pixelpos.y-cy)) # add together

