
from random import randint

MISS = object()

def melee(attacker, target):
    weapon = attacker.equipment.get("weapon")
    weaponstr = 0
    if weapon and weapon.ranged == False:
        weaponstr = weapon.str
    roll = randint(1,10)
    if roll == 10 or roll + attacker.dex >= target.AC:
        return max(1, weaponstr + attacker.str - target.con)
    else:
        return MISS

def ranged(attacker, target, distance):
    weapon = attacker.equipment.get("weapon")
    weaponstr = 0
    if weapon and weapon.ranged == True:
        weaponstr = weapon.str
    roll = randint(1,10)
    if roll == 10 or roll + attacker.dex >= target.AC:
        return max(1, weaponstr + attacker.str - target.con/2)
    else:
        return MISS

def psy_damage(attacker, target, distance):
    if randint(1,10) - distance/2 + attacker.psy >= target.psy:
        return max(0, attacker.psy - target.psy)
    else:
        return MISS
