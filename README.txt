 Geiger A.D. '42
 A rogue-like game written in 7 days for 7DRL 2011. Presented by
  Nigel McBride: Music, Graphics, Design, Sound Effects, Writing
  Ralph Versteegen: Programming, Additional Resources, Design

Setting based on Jonathan Tweet's Omega World (2002).

  Additional help and graphics by Lisa Versteegen
  Built on top of:
   Precise permissive field of view algorithm by Aaron MacDonald
   (A tiny part of) Gummworld2 by Gummbum
   Python by True Hackers
   Pygame, SDL, FreeType, etc.

Press h or ? ingame for the help screen.

This game is written almost completely from scratch. Is it a success, or a failure? The game mechanics is virtually complete, but there is no victory condition, and the game never got final balancing (or the fun random mutations) so play at your own risk. An expanding version completing the original vision may be released in future.

Requirements are, I believe, Python 2.5+ with Pygame 1.9+ and SDL_Mixer. Tested in Python 2.6 on GNU/Linux, Mac OS X, Windows XP
To play (Windows):  geiger42.exe
To play (Other):    python main.py
