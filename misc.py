import math
from random import randint, uniform

UP = 0
UPRIGHT = 1
RIGHT = 2
DOWNRIGHT = 3
DOWN = 4
DOWNLEFT = 5
LEFT = 6
UPLEFT = 7

dirdiffs = ((0,-1), (1,-1), (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1))

def dir_to_diff(dir):
    return dirdiffs[dir]

def sign_int(i):
    if i <= 0:
        return str(i)
    else:
        return "+" + str(i)

def randint2(low, high):
    """Like random.randint returns an integer uniformly in [low, high] if low and high
    are integers, and in general the nearest integer to a real in [low-0.5, high+0.5).
    high is capped to min(low, high),
    otherwise the average is always (low+high)/2"""
    if low > high:
        high = low
    ret = uniform(low, high + 0.99999)
    #return int(min(max(ret, math.ceil(low)), math.floor(high)))
    return int(math.floor(ret))

# Add a tilepos class?
def L1norm(pos1, pos2):
    return abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])

def capped_adjust(stat, cap, adjust_by):
    "cap is a low,high pair; returns amount to adjust (no effect!)"
    oldstat = stat
    newstat = max(cap[0], min(cap[1], stat + adjust_by))
    return newstat - oldstat


def gen_one_of(problist):
    """Generate at most one object (call objects assumed to be classes) from a list randomly,
    else return None.

    problist is a tuple composed of class,probability 'pairs' (not tuples)"""
    r = uniform(0, 1)
    accum = 0
    for a,b in zip(*[iter(problist)]*2):
        accum += b
        if r <= accum:
            return a()
    return None

def gen_with_probs(problist):
    """Generate (call objects assumed to be classes) from a list randomly, returning as a list.

    problist is a tuple composed of a mix of class,probability 'pairs' (not tuples)
    and tuples, which are passed to gen_one_of."""
    ret = []
    i = 0
    while i < len(problist):
        drop = problist[i]
        if isinstance(drop, tuple):
            temp = gen_one_of(drop)
            if temp:
                ret.append(temp)
            i += 1
        else:
            r = uniform(0, 1)
            prob = problist[i + 1]
            if r <= prob:
                ret.append(drop())
            i += 2
    return ret


def incattr(obj, attr, value):
    setattr(obj, attr, getattr(obj, attr) + value)

class AttrView(object):
    "Access an object like a dict"
    def __init__(self, obj):
        self.obj = obj

    def __getitem__(self, name):
        return getattr(self.obj, name)

    def __setitem__(self, name, value):
        return setattr(self.obj, name, value)

    def __delitem__(self, name):
        return delattr(self.obj, name)

    def __contains__(self, name):
        return hasattr(self.obj, name)
