# Attacks and skills

from random import randint, uniform
from maps import Tile
import entity
from text import *
import sfx
from misc import *
from gummworld2 import State, Vec2d


MISS = object()

def look_for_target(pos, diff):
    "Returns (entity/None,dist)"
    m = State.map
    wantpos = pos
    dist = 0
    while 1:
        wantpos = wantpos + diff  #new object!
        wantx,wanty = wantpos
        dist += 1
        if wantx < 0 or wanty < 0 or wantx >= m.width or wanty >= m.height:
            break
        desttile = m.tile(wantpos)
        if desttile.flags & Tile.ISWALL:
            break
        if desttile.entity:
            return desttile.entity, dist
    return None, 0


class Attack(object):
    ranged = False

class Skill(object):
    name = ""
    description = ""



class MeleeAttack(Attack):

    @staticmethod
    def damage(attacker, target):
        weapon = attacker.equipment.get("weapon")
        weaponstr = 2
        if weapon and weapon.ranged == False:
            weaponstr = weapon.str
        elif attacker.isenemy:
            #kludge: add natural weaponry
            weaponstr = 6
        if attacker.arms == 0:
            # headbutt
            weaponstr = 1
        roll = randint(1,10)
        if roll == 10 or roll + attacker.dex + attacker.maim >= target.AC:
            return max(1, randint2(1, weaponstr * (attacker.str / 10.0)) + attacker.mdmg - randint2(0, target.con/2))
        else:
            return MISS
    
    @classmethod
    def perform(cls, attacker, target):
        "returns time spent"
        weapon = attacker.equipment.get("weapon")
        barehand = False
        ticks = attacker.movetime
        sfx.play("Crush8-Bit")
        if weapon and weapon.ranged == False:
            console(attacker, weapon.verb, target, "with", "a", weapon)
            ticks = weapon.speed
        elif weapon and weapon.ranged == True:
            console(attacker, "^hits", target, "with the butt of", attacker, "'s", weapon)
        elif attacker.arms == 0:
            console(attacker, "^headbutts", target)
        else:
            barehand = True
            if attacker == State.player:
                console(attacker, "^hits", target, "with your bare", attacker.hands())
            else:
                console(attacker, "^hits", target)
        result = cls.damage(attacker, target)
        if result == MISS:
            console(attacker, "missed")
        else:
            target.take_damage(result)
            if target.hp > 0:
                if barehand:
                    if uniform(0,1) <= attacker.poison_melee:
                    #if uniform(0,1) <= 0.5:
                        entity.poison(target)
        return ticks

    @classmethod
    def attempt(cls, attacker, wantmove):
        if wantmove[0] == 0 and wantmove[1] == 0:
            return False
        m = State.map
        wantx, wanty = attacker.pos + (wantmove[0], wantmove[1])
        if wantx < 0 or wanty < 0 or wantx >= m.width or wanty >= m.height:
            return False
        desttile = m.tile(wantx, wanty)
        if desttile.entity:
            return cls.perform(attacker, desttile.entity)
        return False



class FireWeapon(Attack):
    ranged = True

    @staticmethod
    def damage(attacker, target, distance):
        weapon = attacker.equipment.get("weapon")
        roll = randint(1,10)
        if roll == 10 or roll + attacker.dex + attacker.raim >= target.AC:
            return max(1, randint(1, weapon.str) - randint2(0, target.con/3))
        else:
            return MISS

    @classmethod
    def perform(cls, attacker, target, distance):
        "returns time spent. weapon must be valid ranged"
        weapon = attacker.equipment.get("weapon")
        ticks = weapon.speed
        console(attacker, "^fires", attacker, "'s", weapon)
        result = cls.damage(attacker, target, distance)
        if result == MISS:
            console(attacker, "missed")
        else:
            target.take_damage(result)
        return ticks        

    @classmethod
    def usable(cls, attacker):
        weapon = attacker.equipment.get("weapon")
        if not weapon:
            return attacker.fail_msg("You're not holding a weapon!")
        if attacker.arms == 0:
            # Shouldn't be possible to get here without arms
            raise Exception("IMPOSSIBLE: You don't have any arms")
        if not weapon.ranged:
            return attacker.fail_msg("You can't fire that!")
        if attacker.hand_type < attacker.POOR_HANDS:   #FIXME KLUDGE
            return attacker.fail_msg("You can't pull the trigger with your", attacker.hands(), "!")
        if hasattr(weapon, 'shots') and weapon.shots == 0:
            return attacker.fail_msg("Your", weapon, "isn't loaded!")
        return True

    @classmethod
    def attempt(cls, attacker, dir):
        if not cls.usable(attacker):
            return 0
        weapon = attacker.equipment.get("weapon")
        if hasattr(weapon, 'shots'):
            weapon.shots -= 1
        if hasattr(weapon, "shoottext"):
            console(weapon.shoottext)
        if hasattr(weapon, "shootsnd"):
            #print weapon.shootsnd
            sfx.play(weapon.shootsnd)
        target, dist = look_for_target(attacker.pos, dir_to_diff(dir))
        if target:
            return cls.perform(attacker, target, dist)
        #print "hit nothing"
        return weapon.speed

    @classmethod
    def player_attempt(cls):
        if not cls.usable(State.player):
            return 0
        dir = State.app.prompt_dir()
        if dir == None:
            console("Cancelled.")
            return 0
        return cls.attempt(State.player, dir)



class PsyAttack(Skill, Attack):
    ranged = True
    name = "Psychic Attack"

    @staticmethod
    def damage(attacker, target, distance):
        if randint(1,10) - distance/2 + attacker.psy >= target.psy:
            return max(0, attacker.psy - target.psy)
        else:
            return MISS

    @classmethod
    def perform(cls, attacker, target, distance):
        "returns time spent."
        result = cls.damage(attacker, target, distance)
        if result == MISS:
            console(target, "^deflects", attacker, "'s", "psychic attack")
        else:
            console(target, "^howls in mental agony")
            target.take_damage(result)
        return 10

    @classmethod
    def attempt(cls, attacker, dir):
        "psy attack"
        if attacker.psy < 5:
            return 0  # fail
        target, dist = look_for_target(attacker.pos, dir_to_diff(dir))
        if target:        
            return cls.perform(attacker, target, dist)
        #print "hit nothing"
        return 0

    @classmethod
    def player_attempt(cls):
        if State.player.psy < 5:
            console("Your mind is too weak to blast anything!")
            return 0
        dir = State.app.prompt_dir()
        if dir == None:
            console("Cancelled.")
            return 0
        ticks = cls.attempt(State.player, dir)
        if not ticks:
            console("That wall ain't giving")
        return ticks



class Dash(Skill):
    name = "Dash"


    @classmethod
    def player_attempt(cls):
        if State.player.stam < 10:
            console("Not enough stamina")
            return 0
