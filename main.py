### Geiger A.D. '42
### 2011 by Ralph Versteegen and Nigel McBride for the 7DRL contest
###

import time
c0_0 = time.clock()
import sys
import os
import random
try:
    import objgraph
    have_objgraph = True
except ImportError:
    have_objgraph = False
import inspect
import gc
import paths

import pygame
# Initialise pygame
import gfx
from gfx import Screen

from pygame.locals import *
from gummworld2 import State, GameClock, Vec2d

import sfx
import items
import maps
#from entity import makeplayer, Entity, TurnList, Buff, Stats, UP, DOWN, LEFT, RIGHT, playerraces
from entity import *
from skills import *
from maps import Map, Tile, Camera
from text import *
import enemies
import mutations

c0_1 = time.clock()

progdir = os.path.abspath(os.path.dirname(sys.argv[0]))
#rand = random.Random()

#player view radius
view_radius = 11

##print timing info
timing = False

play_music = True
if len(sys.argv) > 1 and 's' in sys.argv[1]:
    play_music = False

def decorate_map(m):
    "After map generation, selects correct tile sprite for each tile, and adds variant tiles"
    mw, mh = m.width, m.height
    lastrow = (mh - 1) * mw
    firstrow = mw
    for i, tile in enumerate(m.tiles):
        # Forward facing SWALLs
        # if tile.tileid == maps.T_SWALL:
        #     if i < lastrow and m.tiles[i + mw].tileid != maps.T_SWALL:
        #         tile.set_tileid(maps.T_SWALL_F)
        
        # Add variations
        tiledef = maps.tileinfo[tile.tileid]
        choice = random.uniform(0, 1)
        for accum, tileid in tiledef.variants:
            if accum >= choice:
                tile.set_tileid(tileid)
                break

def place_items(m):
    what = [items.UnknownPistol, items.UnknownPill, items.UnknownMed,items.UnknownPistol, items.UnknownPill, items.UnknownMed, items.UnknownPistol, items.UnknownPill, items.UnknownMed, items.UnknownRifle, items.Dagger, items.Dagger, items.Club, items.Club, items.Lrifle1, items.FoodPill, items.FoodPill, items.RegBoots, items.GoodBoots, items.BodyArmour] + [items.StrMed] * 6 + [items.RegAmmo, items.RegAmmo, items.EAmmo, items.BAmmo] * 3
    what *= 2
    for itemclass in what:
        x, y, tile = m.find_free_tile()
        tile.add_item(items.gen_item(itemclass))

def place_enemies(m):
    probs = (enemies.Leeche, 0.8, enemies.Tsuris, 0.8, enemies.Zuikutis, 0.5, enemies.Slither, 0.5, enemies.WorkerBot, 0.4, enemies.DeathRobot, 0.05, enemies.Raider, 0.5, enemies.LandOctopus, 0.3, enemies.FoulHopper, 0.1)  * 33
#    probs = (enemies.Leeche, 1)

    gened = items.gen_with_probs(probs)
    for enemy in gened:
        x, y, tile = m.find_free_tile()
        m.move_entity(enemy, (x, y))

def gen_map(mw, mh):
    m = Map(mw, mh)

    for y in range(mh):
        for x in range(mw):
            if random.randint(0, 4) == 0:
                m.set_tileid(x, y, maps.T_SWALL)
            else:
                m.set_tileid(x, y, maps.T_FLOOR1)
    for y in range(mh):
        for x in (0, mw-1):
            m.set_tileid(x, y, maps.T_SWALL)
    for x in range(mw):
        for y in (0, mh-1):
            m.set_tileid(x, y, maps.T_SWALL)
    for i in range(8):
        x1 = random.randint(0, mw - 10)
        y1 = random.randint(0, mh - 10)
        for x,y,tile in m.block(x1, y1, x1 + 6, y1 + 6):
            tile.set_tileid(maps.T_FLOOR2)
    px,py = 14,14
    for x,y,tile in m.block(px-2, py-2, px + 3, py + 3):
        try:
            tile.set_tileid(maps.T_FLOOR2)
        except:
            pass
    decorate_map(m)
    place_items(m)
    place_enemies(m)
    return m


DRAW_FRAME = object()
UPDATE_FRAME = object()

def get_events():
    "is this needed?"
    while 1:
        e = pygame.event.poll()
        if e.type == NOEVENT:
            return
        yield e

class UpdateLoop(object):
    delayed_events = []
    delayed_events2 = []
    screenshot_no = 0

    """clock frame speed used to trigger DRAW_FRAME events,
    clock update speed used to check events (30 fps by default)"""
    def __init__(self, frame_speed = None):
        self.frame_speed = frame_speed
        if frame_speed:
            State.clock.max_fps = frame_speed
        self.force_redraw = False
        #self.queued_events = UpdateLoop.delayed_events
        UpdateLoop.delayed_events += UpdateLoop.delayed_events2
        UpdateLoop.delayed_events2[:] = []

    def throw_event_back(self, e):
        UpdateLoop.delayed_events2.append(e)
    
    class CustomEvent(object):
        def __init__(self, what): self.type = what
    
    def run_events(self):
        while len(UpdateLoop.delayed_events):
            yield UpdateLoop.delayed_events.pop(0)
        while True:
            for e in get_events():
                if e.type == KEYDOWN and e.key == K_F10:
                    fname = "screenshot%0d.png" % UpdateLoop.screenshot_no
                    pygame.image.save(Screen.surface, fname)
                    print "Saved", fname
                    UpdateLoop.screenshot_no += 1
                else:
                    yield e
            if not self.force_redraw:
                State.dt = State.clock.tick()
            if State.clock.update_ready():
                yield UpdateLoop.CustomEvent(UPDATE_FRAME)
            if self.force_redraw or (State.clock.frame_ready() and self.frame_speed):
                self.force_redraw = False
                yield UpdateLoop.CustomEvent(DRAW_FRAME)

def waitforkey():
    loop = UpdateLoop()
    for e in loop.run_events():
        if e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                sys.exit()
            elif e.key != K_LALT and e.key != K_LCTRL and e.key != K_RALT and e.key != K_RCTRL:
                return

class Spacer(object):
    "For easy padding in a LinearMenu menu item"
    def __init__(self, width, height = 0):
        self.w, self.h = width, height

    def width(self):
        return self.w

    def height(self):
        return self.h

class LinearMenu(object):
    def __init__(self, size, spacing = 5, select = True):
        "'select' is the default selectability of menu items."
        self.clear()
        if size == None:
            size = pygame.Rect(0, 0, 720, 520)
        self.size = size
        self.spacing = 0
        self.def_select = select
    
    def _layout_item(self, itemnum):
        "Set self.items[itemnum]['h', 'xoffsets'] and self.hsum[itemnum]"
        item = self.items[itemnum]
        offsets = []
        xoff = 25
        height = 0
        rect = self.size.copy()
        for part in item['contents']:
            offsets.append(xoff)
            rect.inflate_ip(-xoff, 0)
            if hasattr(part, 'get_rect'):  # isinstance(part, pygame.Surface):
                size = part.get_rect()
                height = max(height, size.height)
                xoff = size.width
            elif hasattr(part, 'draw'):
                if hasattr(part, 'set_dest'):
                    part.set_dest(Screen.surface, rect)
                height = max(height, part.height())
                xoff = part.width()
            elif hasattr(part, 'width'):
                xoff = part.width()

        height += self.spacing
        if "height" in item:
            height = item['height']
        item['h'] = height
        item['xoffsets'] = offsets
        self.hsum[itemnum + 1] = self.hsum[itemnum] + height

    def add(self, *parts, **kwargs):
        """parts is a list of surfaces or things implementing draw(surface, rect), height(),
        width() and possibly set_dest(surface, rect), which is called if so,
        kwargs is user data plus 'height' and 'select' (selectable, default true) args"""
        self.hsum.append(0)
        self.items.append(dict(contents = parts, **kwargs))
        # Adds 'h' key to items[-1] and sets hsum[-1]
        self._layout_item(len(self.items) - 1)
        if kwargs.get('select', self.def_select):
            self.selectables.append(len(self.items) - 1)
            if len(self.selectables) == 1:
                # Turn into cursor-based menu
                self.cursor = self.selectables[0]    #idx in items
                self.selected = 0    #idx in selectables
            
    def add_to_last(self, *parts):
        "Append items to the last menu item add()ed"
        last = self.items[-1]
        last["contents"] += parts
        self._layout_item(len(self.items) - 1)

    def clear(self):
        "Remove all items"
        self.items = []  # * numitems
        self.hsum = [0]   # * numitems + 1
        self.selectables = []  # * num selectable items
        self.cursor = None
        self.selected = None
        self.topy = 0

    def redraw(self):
        self._loop.force_redraw = True

    # This needed?
    def spacer(self, height = 20):
        self.items.append({'contents':(), 'h':height})
         
    def draw(self, dest, cliprect):
        rect = cliprect.copy()
        old_clip = dest.get_clip()
        dest.set_clip(rect)

        if self.topy > 0:
            dest.blit(gfx.sprites['cursor5'], rect.move(-25, 0).topright)
            
        earlyout = False
        rect.y -= self.topy
        rely = -self.topy
        for i, menuitem in enumerate(self.items):
            if rely >= self.size.height:
                earlyout = True
                break
            if rely + menuitem['h'] > 0:
                destrect = rect.copy()
                for part, xoffset in zip(menuitem['contents'], menuitem['xoffsets']):
                    destrect.x += xoffset
                    destrect.w -= xoffset
                    if isinstance(part, pygame.Surface):
                        size = part.get_rect()
                        yoffset = (menuitem['h'] - size.h) / 2
                        dest.blit(part, destrect.move(0, yoffset))
                    elif hasattr(part, 'draw'):
                        yoffset = (menuitem['h'] - part.height()) / 2
                        part.draw(dest, destrect.move(0, yoffset))
            if i == self.cursor:
                dest.blit(gfx.sprites['cursor2'], rect.move(0, (menuitem['h'] - 12) / 2))
            rect.y += menuitem['h']
            rely += menuitem['h']
        if earlyout:
            dest.blit(gfx.sprites['cursor6'], cliprect.move(-25, -16).bottomright)
        dest.set_clip(old_clip)

    def fixview(self):
        "Fix self.topy after it or self.cursor is changed"
        if len(self.selectables):
            start, end = 0, len(self.items) - 1
            itemnum = self.selectables.index(self.cursor)
            if itemnum > 0:
                start = self.selectables[itemnum - 1]
            if itemnum < len(self.selectables) - 1:
                end = self.selectables[itemnum + 1]

            starty = self.hsum[start]
            endy = self.hsum[end + 1]
            self.topy = min(max(self.topy, endy - self.size.height), starty)
        else:
            self.topy = max(0, min(self.topy, self.hsum[-1] - self.size.height))

    def select(self, itemno):
        "Set .selected"
        self.selected = itemno
        if self.cursor != self.selectables[self.selected]:
            self._loop.force_redraw = True
            self.cursor = self.selectables[self.selected]

    def loop(self):
        keys = K_UP, K_DOWN, K_KP8, K_KP2
        keysteps = -1, 1, -1, 1
        self._loop = UpdateLoop()
        self._loop.force_redraw = True
        for e in self._loop.run_events():
            if e.type == KEYDOWN:
                if len(self.selectables):
                    # cursor based browsing
                    if e.key in keys:
                        self.selected = (self.selected + keysteps[keys.index(e.key)]) % len(self.selectables)
                    elif e.key == K_HOME or e.key == K_PAGEUP:
                        self.selected = 0
                    elif e.key == K_END or e.key == K_PAGEDOWN:
                        self.selected = len(self.selectables) - 1
                    else:
                        yield e
                    self.select(self.selected)
                else:
                    # scrolling browsing
                    if e.key in keys:
                        self.topy += 25 * keysteps[keys.index(e.key)]
                    elif e.key == K_HOME:
                        self.topy = 0
                    elif e.key == K_END:
                        self.topy = self.hsum[-1]
                    elif e.key == K_PAGEUP:
                        self.topy -= (self.size.height - 25)
                    elif e.key == K_PAGEDOWN:
                        self.topy += (self.size.height - 25)
                    else:
                        yield e
                    self._loop.force_redraw = True
                self.fixview()
            else:
                yield e

def select_menu(items):
    """A generic menu for selecting from a bunch of strings/objects.
    Returns index of the string chosen, or None on cancel"""
    screen = Screen.surface
    rect = pygame.Rect(200, 140, 800 - 400, 600 - 280)
    gfx.draw_textbox(rect, 'Red')
    rect.inflate_ip(-30, -30)
    background = screen.copy()

    menu = LinearMenu(rect)
    for item in items:
        menu.add(WrappedText(str(item)))

    for e in menu.loop():
        if e.type == DRAW_FRAME:
            screen.blit(background, (0, 0))
            menu.draw(screen, rect)
            Screen.flip()
        elif e.type == KEYDOWN:
            if e.key == K_RETURN or e.key == K_SPACE:
                return menu.cursor
            elif e.key == K_ESCAPE or e.key == K_q:
                return None
        elif e.type == QUIT:
            sys.exit()

def race_picker():
    outerrect = pygame.Rect(40, 40, 800 - 80, 600 - 80)
    menurect = outerrect.inflate(-90, -100)
    menu = LinearMenu(menurect)
    title = WrappedText("Choose your race...")

    for race in playerraces:
        menu.add(WrappedText(race.racename), race = race)

    screen = Screen.surface
    for e in menu.loop():
        race = menu.items[menu.cursor]['race']
        if e.type == DRAW_FRAME:
            Screen.fill()
            drect = pygame.Rect(100, 400, 800 - 200, 150)
            gfx.draw_textbox(outerrect, 'Red')
            gfx.draw_textbox(drect, 'Red')

            trect = pygame.Rect(40, 40, 170, 10)
            trect.center = (400,20)
            title.draw(screen,trect)

            drect.inflate_ip(-40, -40)

            menu.draw(screen, menurect)

            blurb = race.blurb + "\n"
            for stat,val in race.bonuses.iteritems():                
                blurb += stat + ": " + sign_int(val) + " "

            description = WrappedText(blurb, Color('white'))
            description.draw(screen, drect)
            Screen.flip()

        elif e.type == KEYDOWN:
            if e.key == K_RETURN or e.key == K_SPACE:
                return race
            elif e.key == K_ESCAPE or e.key == K_q:
                sys.exit()

def char_gen_menu():

    text = """Long after civilisation stopped counting the years, life began to repopulate the barren wastes. Small groups of people began to reform communities, and cities, slowing piecing together their history. They began to rebuilt, and reutilise their old, forgotten technologies. That was, until the others began to appear.

Shambling from the deserts and ruins, great hulking shapes began to appear, with the bodies of men and the faces of animals. These creatures were a fearsome sight, but were less of a horror than that which followed them. Twisted and gnarled men with multiple faces, arms, pincers, claws, eyes.

At first there was peace; but then the machines came, bringing death and destruction with them, seeking out all of the mutated life. The pure strain humans, realising that they could not survive with the mutants, forced them out of their communities.

Now, hostility is rife. No one can be trusted; the machines are everywhere. Your settlement has sent you to the ancient facility, in an attempt to secure food, technology and hope. You have be called to brave the depths of this bunker and reach the end of it, where it has been said, that the ultimate technology is housed."""

    if play_music:
        pygame.mixer.music.load("Forever Alone.mp3")
        pygame.mixer.music.play()  #once

    Screen.fill()
    rect = pygame.Rect(40, 40, 800 - 80, 600 - 80)

    gfx.draw_textbox(rect, 'Red')
    rect.inflate_ip(-40, -40)
    to = WrappedText(text)
    to.draw(Screen.surface, rect)
    Screen.flip()
    waitforkey()

    textbox = pygame.Surface((800,600))
    textbox.fill(pygame.Color('black'))
    textbox.set_alpha(60)
    screen = Screen.surface

    for i in range(15):
        screen.blit(textbox, (0, 0))
        Screen.flip()
        pygame.time.wait(100)

    return race_picker()

def generate_defects():
    player = State.player
    defect_list = mutations.roll_mutations(mutations.defects, player.mutations)
    val = sum(m.value for m in player.mutations)
    for mut in defect_list:
        player.defects.append(mut)
        mut.apply(player)
        val -= mut.value
        if val <= 0:
            break
    player.set_derived_stats()

def pick_mutations_menu(player):
    if player.ismutant == False:
        character_menu(True)
        return
    mut_list = mutations.roll_mutations(mutations.mutations)
    val = [0]
    
    def additional_mutation(maxval = 15, limit = 6):
        "Return true if can't add more"
        mut = mut_list.pop(0)
        player.mutations.append(mut)
        mut.apply(player)
        player.set_derived_stats()
        val[0] += mut.value
        return val[0] >= maxval or len(player.mutations) >= limit

    while additional_mutation(8, 6) == False:
        pass

    if val[0] >= 15:
        # already too many mutations, player gets no choice
        generate_defects()
        character_menu(True)
    else:
        character_menu(True, True, additional_mutation)        


def character_menu(mutating = False, adding_mutations = False, additional_mutation = None):
    "The character info screen, also for picking mutations"
    screen = Screen.surface
    player = State.player
    State.app.draw_map()
    rect = pygame.Rect(40, 40, 800 - 80, 600 - 80)
    gfx.draw_textbox(rect, 'Red')
    rect.inflate_ip(-40, -40)
    background = screen.copy()

    headercol = (140, 200, 140)

    mut_rect = pygame.Rect(60, 170, 800 - 100, 600 - 80)
    descrip_rect = pygame.Rect(60, 482, 800 - 100, 600)
    commands_rect = Rect(50, 515, 700, 0)
    note_rect = Rect(50, 440, 700, 0)

    commands0 = WrappedText("\nenter/esc/q - exit", font = emph_font, colour = 'White')
    commands1 = WrappedText("\nenter/esc/q - exit     up/down - browse mutations", font = emph_font, colour = 'White')
    if mutating and adding_mutations:
        commands2 = WrappedText("up/down - browse mutations \n"
                                "space - roll another beneficial mutation    enter/esc - stop and roll defects", font = emph_font, colour = 'White')
        note_text = WrappedText("You may add additional mutations. Note that you recieve defects with total value at least the total value of your beneficial mutations")

    stats_list = LinearMenu(None, 5, False)   # default to non-selectable    
    mutations_menu = LinearMenu(mut_rect, 0)

    def build_stats_list():
        stats_list.clear()
        stats_list.add(WrappedText(player.name))
        stats_list.add(WrappedText("HP: " + str(player.hp) + "/" + str(player.maxhp), width = 150),
                       WrappedText("MP: " + str(player.mp) + "/" + str(player.maxmp), width = 150),
                       WrappedText("Stamina: " + str(player.stam) + "/" + str(player.maxstam), width = 150))

        if mutating:
            headers2 = ["Base:", "Mutations:"]
        else:
            headers2 = ["Base:", "Temp:"]
        headers = ([Spacer(width = 104)] + [WrappedText(text, anchor = WrappedText.CENTRE, width = 108,
                                                        colour = headercol)
                                            for text in headers2]) * 2
        stats_list.add(*headers)

        parity = [0]
        def add_stat_info(name, base, change):
            if parity[0] == 0:
                stats_list.add()
            #else:
            #    stats_list.add_to_last(Spacer(width = 100))
            parity[0] ^= 1
            stats_list.add_to_last(WrappedText(name + ":", width = 100),
                                   WrappedText(base, width = 110, anchor = WrappedText.CENTRE),
                                   WrappedText(change, width = 110, anchor = WrappedText.CENTRE))


        if mutating:
            statset = player.racestats
        else:
            statset = player.basestats
        for stat, statattr in zip(Stats.statnames, Stats.statattrs):
            baseval = getattr(statset, statattr)
            newval = getattr(player, statattr)
            change = ""
            if newval != baseval:
                change = sign_int(newval - baseval)
            add_stat_info(stat, str(baseval), change)

        # Special case Speed
        baseval = statset.speed_percent()
        newval = player.speed_percent()
        change = ""
        if newval != baseval:
            change = sign_int(newval - baseval) + "%"
        add_stat_info("Speed", str(baseval) + "%", change)


    def add_mutations_to_menu(mut_list, title):
        mutations_menu.add(WrappedText(title, colour = headercol, font = emph_font, width = 350),
                           WrappedText("Value:", colour = headercol, anchor = WrappedText.CENTRE),
                           height = 25, select = False)
        val = 0
        for m in mut_list:
            mutations_menu.add(WrappedText(m.name, width = 350), 
                               WrappedText(str(m.value), anchor = WrappedText.CENTRE),
                               mutation = m)
            val += m.value
        if not mut_list:
            mutations_menu.add(WrappedText("None"), height = 25, select = False)
        mutations_menu.add(Spacer(width = 200),
                           WrappedText("Total value:", colour = headercol, width = 150),
                           WrappedText(str(val), anchor = WrappedText.CENTRE),
                           select = False)

    def build_mutations_menu(with_defects):
        mutations_menu.clear()
        add_mutations_to_menu(player.mutations, "Mutations:")
        if with_defects:
            add_mutations_to_menu(player.defects, "Defects:")

    build_stats_list()
    build_mutations_menu(adding_mutations == False)

    for e in mutations_menu.loop():
        if e.type == DRAW_FRAME:
            screen.blit(background, (0, 0))
            stats_list.draw(screen, rect)
            mutations_menu.draw(screen, mut_rect)

            if mutations_menu.cursor:
                mutation = mutations_menu.items[mutations_menu.cursor]['mutation']
                descrip = WrappedText(mutation.full_description(), colour = Color(200, 200, 0))
                descrip.draw(screen, descrip_rect)

                commands = commands1
                if mutating and adding_mutations:
                    commands = commands2
                    note_text.draw(screen, note_rect)
            else:
                commands = commands0
            commands.draw(screen, commands_rect)

            Screen.flip()
        elif e.type == KEYDOWN:
            if adding_mutations:
                if e.key in (K_RETURN, K_q, K_ESCAPE):
                    generate_defects()
                    adding_mutations = False
                elif e.key == K_SPACE:
                    if additional_mutation():
                        # No more choices
                        generate_defects()
                        adding_mutations = False
                else:
                    continue
                build_stats_list()
                selected = mutations_menu.selected
                build_mutations_menu(adding_mutations == False)
                mutations_menu.select(selected)
                mutations_menu.redraw()
            elif e.key in (K_RETURN, K_q, K_ESCAPE):
                return
        elif e.type == QUIT:
            sys.exit()

def scrollback_menu():
    screen = Screen.surface
    State.app.draw_map()
    rect = pygame.Rect(40, 40, 800 - 80, 600 - 80)
    gfx.draw_textbox(rect, 'Red')
    rect.inflate_ip(-40, -40)
    background = screen.copy()

    menu = LinearMenu(rect, 0)
    for msg in State.console.messages:
        # Re-wrap the message. Console will reset the rects on any strings it draws
        #msg.set_dest_width(rect)
        menu.add(msg, select = False)
    menu.topy = menu.hsum[-1]
    menu.fixview()

    for e in menu.loop():
        if e.type == DRAW_FRAME:
            screen.blit(background, (0, 0))
            menu.draw(screen, rect)
            Screen.flip()
        elif e.type == KEYDOWN:
            if e.key in (K_RETURN, K_SPACE, K_q, K_ESCAPE):
                return
        elif e.type == QUIT:
            sys.exit()

def item_menu():
    player = State.player
    descrip_box_y = 600 - 40 - 60
    menurect = pygame.Rect(40, 80, 800 - 80, descrip_box_y - 15 - 80).inflate(-40, -40)
    menu = LinearMenu(menurect)
    itemlist = []

    white = Color('White')

    if player.isgoo:
        title = WrappedText("You have absorbed:", white)
    else:
        title = WrappedText("You are carrying:", white)

    invent = player.inventory
    if len(invent) == 0:
        menu.add(WrappedText("Nothing!"), select=False)
    else:
        for category in items.categories:
            stuff = [thing for thing in invent if thing.category == category]
            if len(stuff):
                menu.add(WrappedText(category + ":"), select=False)
                for thing in stuff:
                    textlist = [False, "a", thing]
                    if player.has_equipped(thing):
                        textlist.append("  (equipped)")
                    if isinstance(thing, items.Ammo):
                        textlist.append("  (" + str(thing.amount) + " shots)")
                    if hasattr(thing, 'magazine'):
                        textlist.append("  (" + str(thing.shots) + "/" + str(thing.magazine) + " shots remaining)")
                    menu.add(thing.img, WrappedText(form_msg(*textlist)), item = thing, height = 30)

    commands1 = WrappedText("up/down - select    d - drop    u - use", white, emph_font)
    commands2 = WrappedText("up/down - select    d - drop    u - use    r - remove", white, emph_font)

    for e in menu.loop():
        item = None
        if menu.cursor is not None:
            item = menu.items[menu.cursor].get('item')

        if e.type == DRAW_FRAME:
            screen = Screen.surface
            Screen.fill()
            State.app.draw_map()

            rect = pygame.Rect(40, 40, 800 - 80, 600 - 80)
            gfx.draw_textbox(rect, 'Red')
            pygame.draw.line(screen, Color('Red'), (40, descrip_box_y), (800 - 40, descrip_box_y), 4)
            rect.inflate_ip(-40, -40)
            title.draw(screen, rect)

            menu.draw(screen, menurect)

            if item and player.has_equipped(item):
                commands = commands2
            else:
                commands = commands1
            commands.draw(screen, pygame.Rect(800 - 40 - 25 - commands.width(), descrip_box_y - 25, 800, 600))

            if item:
                rect = pygame.Rect(40, descrip_box_y, 800 - 80, 600 - 40 - descrip_box_y)
                rect.inflate_ip(-40,-40)
                rect.y -= 5
                description = WrappedText(item.get_description())
                description.draw(screen, rect)

            Screen.flip()

        elif e.type == KEYDOWN:
            if e.key == K_d:
                if item:
                    return player.try_drop(item)
            elif e.key == K_r:
                if item and player.has_equipped(item):
                    return player.try_unequip(item)
            elif e.key == K_u:
                if item:
                    #print "using item with use:,", item.equip_as
                    if item.category == "Consumables":
                        player.inventory.remove(item)
                        return player.consume(item)
                    elif item.equip_as:
                        return player.try_equip(item)
                    else:
                        console("You can't use that")
                        return 0
            elif e.key not in (K_LALT, K_LCTRL, K_RALT, K_RCTRL):
                return 0 #time passed


def gameover_screen():
    if play_music:
        pygame.mixer.music.load("Forever Alone.mp3")
        pygame.mixer.music.play()  #once
    Screen.fill()
    rect = pygame.Rect(100, 100, 800 - 80, 600 - 80)
    content = """GAME OVER"""
    text = WrappedText(content)
    text.draw(Screen.surface, rect)
    Screen.flip()
    waitforkey()
    sys.exit()

def help_menu():
    Screen.fill()
    State.app.draw_map()
    rect = pygame.Rect(40, 40, 800 - 80, 600 - 80)
    gfx.draw_textbox(rect, 'Blue')
    rect.inflate_ip(-40, -40)

    content = """    ~~~ HELP ~~~

numpad   -- Move
hjklyubn -- Move
arrows   -- Move
. space numpad5
         -- Wait a turn
f        -- Fire weapon
r        -- Reload weapon
p        -- Psychic attack
? F1     -- This screen
g ,      -- Pick up item
m        -- Show messages
i        -- Inventory
c        -- Character info screen
d        -- Debug options
q        -- Quit
F10      -- Take a screenshot (saved as 'screenshotXX.png')

Equip, use, and drop items from the inventory.

Move your mouse over an object or creature to see a description"""
    text = WrappedText(content)
    text.draw(Screen.surface, rect)
    Screen.flip()
    waitforkey()

msgs1 = """You hear a distant scuffling
You hear the whirring of some gears
You hear the pump of a hydraulic ram
You hear a low, powerful hum
You hear some muffled grunts
You hear water dripping
You hear a distant scream
You hear metal clanking
You hear distant laughing
You hear footsteps
You hear slithering
You hear a distant banging
You hear a loud click
You hear some distant music
You see some small pools of dirty water
You see some litter strewn on the ground
You see thick layers of dust everywhere
You see wet tracks on the ground
You see a pile of empty shell cases
This must have been a science facility long ago
There must be lots of old technology here
This place seems to have been sheltered from the radiation shocks
There is a draft from the wastes being blown inside"""

goomsgs = """You absorb some old crumbs from the ground -- you enjoyed them
You start to dissolve the ground beneath you
You feel your consistency change slightly
You wobble a little
You accidentally bifurcate and reabsorb yourself"""

class RandomMsg(object):
    def __init__(self):
        State.turnlist.add(self, 60)

    def take_turn(self):
        basmsg = msgs1.split("\n") 
        if State.player.isgoo:
            moremsg = goomsgs
        moremsg = State.player.msgs.split("\n")
        r =random.randint(0, len(basmsg) + len(moremsg) - 1)
        if r < len(basmsg):
            msg = basmsg[r]
        else:
            msg = moremsg[r - len(basmsg)]
        console(msg)
        return random.randint(300,800)

class App():
    def __init__(self):
        c0 = time.clock()
        Screen.init((800, 600), "Geiger A.D. '42")

        State.app = self

        pygame.mixer.init(frequency=22050, size=-16, channels=2, buffer=4096)


        State.clock = GameClock(30, 30)

        self.were_movements = False
        self.frame_number = 0
        self.slide_speed = 0.25
        State.turnlist = TurnList()
        self.num_queued_moves = 0
        State.gameover = False
        self.tooltip = None
        self.tooltippos = None
        self.mouseredraw = False

        c1 = time.clock()
        gfx.load(gfx.sprites, os.path.join(progdir,"Graphics"), True)
        gfx.load(gfx.tiles, os.path.join(progdir,"Graphics","tiles"))
        gfx.load(gfx.items, os.path.join(progdir,"Graphics","items"), True)
        c2 = time.clock()
        gfx.darkened_gfx(gfx.tiles, gfx.tiles, "_dark", 70)
        gfx.darkened_gfx(gfx.items, gfx.items, "_dark", 70)
        c3 = time.clock()
        maps.init_tiles()
        c4 = time.clock()
        State.map = gen_map(200, 200)
        c5 = time.clock()

        initialpos = Vec2d(14, 14)

        State.map.update_fov(initialpos, view_radius)
        State.map.layers.append(State.map.create_empty_maplayer())
        State.map.layers.append(State.map.create_empty_maplayer())
        State.camera = Camera()
        State.camera.setcentre(initialpos * 40)
        State.map.update_tilelayers()
        State.map.update_itemlayer(1)
        c6 = time.clock()
        sfx.load_sounds(os.path.join(progdir, "sfx2"))
        c7 = time.clock()


        #pygame.mixer.Sound('sfx2/clink1.ogg').play()
        sfx.play("Crush8-Bit")


        if len(sys.argv) > 1 and 'f' in sys.argv[1]:
            if 'ff' in sys.argv[1]:
                self.player = makeplayer(Human)
            else:
                self.player = makeplayer(MutantHuman)
            self.player.inventory = [A() for A in [items.Antidote, items.RegAmmo, items.Lrifle1, items.EAmmo, items.Fpistol] * 3]
        else:
            self.player = makeplayer(char_gen_menu())
        State.player = self.player
        pick_mutations_menu(self.player)

        self.randoms = RandomMsg()

        State.map.move_entity(self.player, initialpos)

        State.console = Console()
        c8 = time.clock()

        if timing:
            print "***Startup times***"
            print "modules: %f" % (c0_1 - c0_0)
            print "__main__ init: %f" % (c0_2 - c0_1)
            print "psyco: %f" % (c0_3 - c0_2)
            print "pygame startup: %f" % (c1 - c0)
            print "load gfx: %f" % (c2 - c1)
            print "darkened gfx: %f" % (c3 - c2)
            print "gen_map: %f" % (c5 - c4)
            print "update_fov & create_maplayer: %f" % (c6 - c5)
            print "load sound: %f" % (c7 - c6)
            print "other startup: %f" % (c8 - c7)
            print "TOTAL: %f" % (c8 - c0_0)
            print
            
        try:
            if play_music:
                pygame.mixer.music.load("Mutate Alone.mp3")
                pygame.mixer.music.play(-1)  #forever
        except Exception:
            pass

        console("You become self aware!")
        console("You feel threatened with imminent death...")

    def mouse_move(self, pos):
        self.tooltip = None
        mappos = State.camera.screen_to_tile(pos)
        ##print "mappos=", mappos
        try:
            tile = State.map.tile(mappos)
        except IndexError:
            return
        box = pygame.Surface((400, 100))
        box.fill(pygame.Color('black'))
        box.set_alpha(200)

        rect = pygame.Rect(0,0,400,100)
        pygame.draw.rect(box, pygame.Color('Blue'), rect, 3)
        rect.inflate_ip(-10,-10)
        if tile.flags & Tile.VISIBLE and tile.entity:
            text = tile.entity.name + "\n"
            #for stat, statattr in zip(Stats.statnames, Stats.statattrs):
            for stat in Stats.all_statattrs:
                text += stat + ":" + str(getattr(tile.entity, stat)) + " "
            text += "\n" + tile.entity.descr
            textobj = WrappedText(text, pygame.Color(200,200,240), small_font)
        elif tile.flags & Tile.SEEN and tile.contents:
            if len(tile.contents) > 1:
                text = form_msg("A pile of items\nTopmost is", "a", tile.contents[0])
            else:
                text = form_msg("a", tile.contents[0], "lies here")
            textobj = WrappedText(text, pygame.Color(200,200,240), small_font)
        else:
            #textobj = WrappedText("Nothing of interest...", pygame.Color(200,200,240), small_font)
            #textobj = WrappedText("Nothing of interest... z="+str(tile.z), pygame.Color(200,200,240), small_font)
            return
        textobj.draw(box, rect)
        self.tooltip = box
        if pos[1] > 600 - rect.height - 15:
            self.tooltippos = (min(pos[0], 800 - rect.width), pos[1] - rect.height)
        else:
            self.tooltippos = (min(pos[0], 800 - rect.width), pos[1] + 15)
        self.mouseredraw = True

    def update_mouse(self, e):
        "Called on events when on the map to process mouse events. The game could be in any state!"
        typ = e.type
        if typ == MOUSEMOTION:
            #self.on_mouse_motion(e.pos, e.rel, e.buttons)
            pass
        elif typ == MOUSEBUTTONUP:
            #self.on_mouse_button_up(e.pos, e.button)
            pass
        elif typ == MOUSEBUTTONDOWN:
            #self.on_mouse_button_down(e.pos, e.button)
            pass

    def logic_loop(self):
        #the REAL game loop
        time0 = 0
        aimoves = 0
        while 1:
            State.console.new_context()
            # The things returned may be Entitys or Buffs
            nextmove = State.turnlist.get_next()
            ##print id(nextmove)
            if nextmove == self.player:
                #draw movements if any
                if timing:
                    print time.clock() - time0, "for updates,", aimoves, "ai turns"
                if self.were_movements:
                    self.were_movements = False
                    self.move_animate()
                    ##print "sliding fps: ", self.frame_number/(time.clock() - self.frame_time)
                else:
                    self.draw()

                State.console.time_step()
                Entity.take_turn(self.player)
                #Wait for player input, and keep redrawing for mouse stuff
                ticks = self.player_turn()
                time0 = time.clock()
                aimoves = 0
            else:
                #run AI until next player turn
                ticks = nextmove.take_turn()
                aimoves += 1
            ##print "time:", ticks
            State.turnlist.add_time(nextmove, ticks)

            if State.gameover:
                pygame.mixer.music.fadeout(2000)  #meant to block (ugh) but doesn't?
                if self.were_movements:
                    self.were_movements = False
                    self.move_animate()
                self.draw()
                waitforkey()
                gameover_screen()

    def move_animate(self):
        loop = UpdateLoop(30)
        slide = 0.0
        self.frame_time = time.clock()
        self.frame_number = 0
        self.draw(slide)
        speed = 0.25
        self.num_queued_moves = max(0, self.num_queued_moves - 1)
        for e in loop.run_events():
            if e.type == DRAW_FRAME:
                ##print "at " + str(time.clock()) +" slide = " + str(slide)
                slide += self.slide_speed
                self.draw(slide)
                if slide >= 0.999:
                    State.map.clean_slides()
                    return
            elif e.type == KEYDOWN:
                if e.key in (K_LEFT, K_RIGHT, K_DOWN, K_UP):
                    if self.num_queued_moves <= 1:
                        loop.throw_event_back(e)
                        self.num_queued_moves += 1
                    else:
                        self.slide_speed = 0.5
                else:
                    loop.throw_event_back(e)
            else:
                loop.throw_event_back(e)

    def player_turn(self):
        self.playerloop = UpdateLoop(5)
        ticks_waited = 0
        try:
            for e in self.playerloop.run_events():
                if e.type == DRAW_FRAME:
                    self.draw()
                    ticks_waited += 1
                    if ticks_waited >= 2:
                        self.slide_speed = 0.25   #revert to default
                elif e.type == UPDATE_FRAME: 
                   self.update()
                else:
                    State.console.new_context()
                    typ = e.type
                    if typ == KEYDOWN:
                        ticks = self.on_key_down(e.unicode, e.key, e.mod)
                        if ticks:
                            return ticks
                    elif typ == MOUSEMOTION:
                        #possibly redraws
                        self.mouse_move(e.pos)
                    elif typ == QUIT:
                        sys.exit()
        finally:
            del self.playerloop
            self.tooltip = None

    def prompt_key(self, keys = None, values = None):
        self.draw()
        loop = UpdateLoop(5)
        for e in loop.run_events():
            if e.type == DRAW_FRAME:
                self.draw()
            elif e.type == UPDATE_FRAME: 
               self.update()
            else:
                typ = e.type
                if typ == KEYDOWN:
                    if keys and e.key in keys:
                        if values:
                            return values[keys.index(e.key)]
                        else:
                            return e.unicode
                    elif e.key != K_LALT and e.key != K_LCTRL and e.key != K_RALT and e.key != K_RCTRL:
                        if keys == None:
                            return e.unicode
                        return None
                elif typ == MOUSEMOTION:
                    #possibly redraws
                    self.mouse_move(e.pos)
                elif typ == QUIT:
                    sys.exit()

    def update(self):
        if self.mouseredraw:
            self.draw()
            self.mouseredraw = False
        
    def draw_map(self, slide=1.0):
        State.map.layers[0].draw()
        State.map.layers[1].draw()
        State.map.draw_entitylayer(slide)

    def draw_hud(self):
        rect = pygame.Rect(0, 0, 800, 80)
        textbox = pygame.Surface((rect.width, rect.height))
        textbox.fill(pygame.Color('black'))
        textbox.set_alpha(200)
        Screen.blit(textbox, rect)

        play = self.player
        text = "HP:" + str(play.hp) + "/" + str(play.maxhp) + " "
        text += " MP:" + str(play.mp) + "/" + str(play.maxmp) + " "
        text += " Stamina:" + str(play.stam) + "/" + str(play.maxstam) + " "
        for stat, statattr in zip(Stats.statnames, Stats.statattrs):
            text += stat + ":" + str(getattr(play, statattr)) + " "
        rect = pygame.Rect(0, 0, 180, 80)
        to = WrappedText(text)
        to.draw(Screen,rect)

        State.console.draw()

    def draw(self, slide=1.0):
        State.map.update_itemlayer(1)  #MEH
        self.frame_number += 1
        self.player.calc_pixelpos(slide)
        State.camera.setcentre(self.player.pixelpos)
        Screen.fill()
        self.draw_map(slide)
        self.draw_hud()
        if self.tooltip:
            Screen.blit(self.tooltip, self.tooltippos)
        Screen.flip()
        
    def player_movement(self, wantmove):
        newpos = self.player.try_move(wantmove)
        if newpos:
            tile = State.map.tile(*newpos)
            #NOTE: anything that was consumed when moving onto the tile won't be printed
            if tile.contents:
                thing_list = ["You see"]
                for thing in tile.contents:
                    if len(thing_list) > 1:
                        thing_list.append(", ")
                    thing_list += ["a", thing]
                thing_list.append("here")
                console(*thing_list)
            State.camera.setcentre(self.player.pos * 40)
            c1 = time.clock()
            State.map.update_fov(self.player.pos, view_radius)
            c2 = time.clock()
            State.map.update_tilelayers()
            State.map.update_itemlayer(1) 
            State.map.update_distmap()
            c3 = time.clock()
            if timing:
                print "update_fov: %f" % (c2 - c1)
                print "update_tile/itemlayer: %f" % (c3 - c2)
            self.playerloop.force_redraw = True
            return self.player.movetime
        return MeleeAttack.attempt(self.player, wantmove)
        #return 0

    def try_pickup(self):
        tile = State.map.tile(*self.player.pos)
        if tile.contents:
            thing = tile.contents[0]
            if len(self.player.inventory) >= 20:
                console("You can't carry any more!")   #FIXME: can pick ammo and combine
                return 0
            # identify anything you pick up
            if not thing.flags & items.Item.CANT_IDENTIFY:
                thing.flags |= items.Item.IDENTIFIED
            return self.player.try_pickup(thing)
        else:
            console("There's nothing to pick up here.")
            return 0

    def debug_menu(self):
        self.dbg_type = 'tuple'
        options = "G:Goo, E:exhaust, C:garbage collect, I:spawn item, S:spawn enemy"
        if have_objgraph:
            options += ", T:common types, O:new objects, R:weakref refs, B:backrefs for new " + self.dbg_type + "s, U:update " + self.dbg_type + " lists"
        console("Which debug function? [" + options + "]")
        ret = self.prompt_key()
        if have_objgraph:
            if ret == "t":
                print "----Most common types----"
                objgraph.show_most_common_types()
            elif ret == "o":
                print "----New objects----"
                objgraph.show_growth(limit=40)                
            elif ret == "r":
                objgraph.show_chain(
                    objgraph.find_backref_chain(
                        random.choice(objgraph.by_type('weakref')),
                        inspect.ismodule),
                    filename=('weakref%d.png' % randint(1,100)))
            elif ret == "b":
                def printo(o):
                    if isinstance(o, tuple) or isinstance(o, dict):
                        return repr(o)[:60]
                    else:
                        return ""
                newobjs = set(id(obj) for obj in self._newobjs) - set(id(obj) for obj in self._oldobjs)
                objgraph.show_backrefs([objgraph.at(newobjs.pop())], max_depth = 4, 
                                       extra_ignore=(id(self._newobjs),), refcounts=True, 
                                       filename=(self.dbg_type + 's%d.png' % randint(1,100)), extra_info=printo)
                del newobjs
            elif ret == "u":
                if hasattr(self, '_newobjs'):
                    self._oldobjs = self._newobjs
                self._newobjs = objgraph.by_type(self.dbg_type)
                print len(self._newobjs), self.dbg_type, "objects found"
        if ret == "g":
            turning_into_goo(State.player)
        elif ret == "c":
            print "Garbage collecting; objects before:", gc.get_count()
            gc.collect()
            print "objects after:", gc.get_count()
        elif ret == "e":
            self.player.stam = 0
            self.player.mp = 0
        elif ret == "i":
            itemno = select_menu(items.all_items)
            if itemno != None:
                State.map.tile(State.player.pos).add_item(items.all_items[itemno]())
        elif ret == "s":
            enemyno = select_menu(enemies.all_enemies)
            if enemyno != None:
                ppos = State.player.pos
                for x,y,tile in State.map.block(*(list(ppos + (-3, -3)) + list(ppos + (3, 3)))):
                    if tile.is_free():
                        State.map.move_entity(enemies.all_enemies[enemyno](), (x, y))
                        break
        else:
            console("Cancelled/Unavailable.")
        if False:
            gc.set_debug(gc.DEBUG_LEAK)
            print gc.garbage


    dirkeys = K_UP, K_RIGHT, K_LEFT, K_DOWN, \
        K_KP8, K_KP9, K_KP6, K_KP3, K_KP2, K_KP1, K_KP4, K_KP7, \
        K_h, K_j, K_k, K_l, K_y, K_u, K_b, K_n
    dirkeymap = UP, RIGHT, LEFT, DOWN, \
        0, 1, 2, 3, 4, 5, 6, 7, \
        LEFT, DOWN, UP, RIGHT, UPLEFT, UPRIGHT, DOWNLEFT, DOWNRIGHT

    def prompt_dir(self):
        "return direction, or None"
        console("In which direction?")
        return self.prompt_key(App.dirkeys, App.dirkeymap)

    # FIXME: always set force_redraw?
    def on_key_down(self, unicode, key, mod):
        if key in App.dirkeys:
            wantmove = dir_to_diff(App.dirkeymap[App.dirkeys.index(key)])
            return self.player_movement(wantmove)
        elif key == K_PERIOD or key == K_SPACE or key == K_KP5:
            return 10 #wait
        elif key == K_ESCAPE or key == K_q:
            sys.exit()
        elif key == K_g or key == K_COMMA:
            return self.try_pickup()
        elif key == K_m:
            scrollback_menu()
            self.playerloop.force_redraw = True
        elif key == K_F1 or unicode == "?":
            help_menu()
        elif key == K_d:
            self.debug_menu()
            self.playerloop.force_redraw = True
        elif key == K_f:
            self.playerloop.force_redraw = True
            return FireWeapon.player_attempt()
        elif key == K_r:
            self.playerloop.force_redraw = True
            return self.player.try_reload()
        elif key == K_p:
            self.playerloop.force_redraw = True
            return PsyAttack.player_attempt()
        elif key == K_i:
            ret = item_menu()
            self.playerloop.force_redraw = True
            return ret
        elif key == K_c:
            character_menu()
            self.playerloop.force_redraw = True
        elif key == K_v:
            character_menu(True)
            self.playerloop.force_redraw = True
        time1 = time.clock()
        if timing:
            print "player_movement:", time.clock() - time1
        return 0
        
if __name__ == '__main__':
    c0_2 = time.clock()
    if 1:
        try:
            import psyco
            psyco.full(memory = 1000)
            psyco.profile(time = 300)
            #print "Using psyco!"
        except ImportError:
            pass
    c0_3 = time.clock()
    if 1:
        #print progdir
        app = App()
        if False:
            cProfile.run('app.run()', 'profile.dat')
        else:
            app.logic_loop()
