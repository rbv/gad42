import math
from maps import *
from entity import *
from items import *
from random import randint, uniform
from skills import *

class Enemy(Entity):
    isenemy = True
    active = False
    def __init__(self):
        super(Enemy, self).__init__(self.graphic)

        baseset = self.racestats
        for k, v in self.stats.iteritems():
            if isinstance(v, tuple):
                val = randint(v[0], v[1])
            else:
                val = v
            setattr(baseset, k, val)

        baseset.maxhp = baseset.hp
        self.set_derived_stats()

        self.inventory = gen_with_probs(self.drops)

        for item in self.inventory:
            if item.equip_as:
                if item.equip_as == "weapon" and item.ranged:
                    continue   #skip
                if self.equipment.get(item.equip_as) == None:
                    self._equip(item)

    def estimates(self, entity, dist):
        def scrub(dmg):
            if dmg == MISS: return 0
            return dmg
        pdmg = 0
        mdmg = 0
        for i in range(2):
            pdmg += scrub(PsyAttack.damage(self, entity, dist)) - scrub(PsyAttack.damage(entity, self, dist))
            mdmg += scrub(MeleeAttack.damage(self, entity)) - scrub(MeleeAttack.damage(entity, self))
        return pdmg, mdmg

    def try_attack(self):
        px,py = State.player.pos
        x,y = self.pos
        angle = math.atan2(py - y, px - x)
        if angle % (math.pi/4) == 0:
            diff = int(round(math.cos(angle))), int(round(math.sin(angle)))
            entity, dist = look_for_target(self.pos, diff)
            if entity == State.player:
                if dist > 1:
                    #prefer psy
                    pdmg,mdmg = self.estimates(State.player, dist)
                    if self.psy >= 5 and pdmg >= mdmg and pdmg >= -7:
                        return PsyAttack.perform(self, State.player, dist)
                    #move closer
                else:
                    return MeleeAttack.perform(self, State.player)
            #move closer
                    
    def take_turn(self):
        Entity.take_turn(self)
        m = State.map
        tile = m.tile(self.pos)
        if self.active or tile.flags & Tile.VISIBLE:
            self.active = True
            ret = self.try_attack()
            if ret: return ret

            #if L1norm(State.player.pos, self.pos) == 1:
            #    return MeleeAttack.perform(self, State.player)
            
            run = False
            if self.hp < self.maxhp / 5:
                run = True
                #print "running away!"

            movediff = State.map.pathfind_step(self.pos, State.player.pos, run)
            if self.try_move(movediff):
                return self.movetime
        return 10  # turn wasted


###########################   ENEMIES   ###############################

class Leeche(Enemy):
    name = "leeche"
    stats = {'hp': (6,12), 'str': 10, 'dex': 15, 'con': 6, 'psy': 1, 'AC': 2, 'exp': 1}
    drops = (HasteMed, 0.05)
    graphic = "fly1"
    descr = "A giant, chattering fly. Tremendous chitin lances protrude from its jaw." 
    poison_melee = 0.10
    hand_name = "tarsal claw"
    feet_name = "tarsomeres"
    hand_type = Entity.NO_FINGERS

class Zuikutis(Enemy):
    name = "zuikutis"
    stats = {'hp':  (6,18), 'str': 16, 'dex': 8, 'con': 18, 'psy': 2, 'AC': 6, 'exp': 8}
    drops = (HasteMed, 0.02, CoF, 0.1, RegBoots, 0.04)
    graphic = "hopper1"
    descr = "A massive, and easily tamed rabbit. These are used by settlers as work animals and mounts."
    hand_name = "paw"
    hand_type = Entity.NO_FINGERS

class Tsuris(Enemy):
    name = "tsuris"
    stats = {'hp':  (12,20), 'str': 12, 'dex': 8, 'con': 10, 'psy': 4, 'AC': 6, 'exp': 4}
    drops = (Pistol, 0.01, CoF, 0.15, Antidote, 0.3)
    graphic = "orlen2"
    descr = "A wasteland people, with two heads. Usually peaceful, but this one is part of a raiding gang."

class Slither(Enemy):
    name = "slither"
    stats = {'hp':  (22,32), 'str': 12, 'dex': 9, 'con': 13, 'psy': 5, 'AC': 9, 'exp': 12}
    drops = ((Brifle1, 0.35, Lrifle1, 0.015), CoF, 0.2)
    graphic = "snakeman1"
    descr = "Fearsome snake-men. Extremely violent and cunning."   

class WorkerBot(Enemy):
    name = "worker bot"
    stats = {'hp':  ( 20,22), 'str': 18, 'dex': 8, 'con': 11, 'psy': 0, 'AC': 9, 'exp': 10}
    drops = (StrMed, 0.05, Dagger, 0.03, Pistol, 0.05)
    graphic = "robot1"
    descr = "A fully functioning robot. Before the end of the world they were obedient, now they hunt out all natural lifeforms." 

class DeathRobot(Enemy):
    name = "death robot"
    stats = {'hp':  (200,400), 'str': 40, 'dex': 40, 'con': 40, 'psy': 0, 'AC': 40, 'exp': 200}
    drops = (StrMed, 1, Rifle, 1.0)
    graphic = "death_robot"
    descr = "A humongous robot from before the end of the world. This giant machine is bristling with weapons, it's only purpose is to destroy anything it finds completely. Worshipped by some waste-cults."
    
class Raider(Enemy):
    name = "raider"
    stats = {'hp':  (10,14), 'str': 6, 'dex': 4, 'con': 12, 'psy': 4, 'AC': 8, 'exp': 6}
    drops = (HasteMed, 0.05, Club, 0.1, Antidote, 0.3)
    graphic = "human2"
    descr = "A pure strain human, probably trying to exterminate mutants."  

class LandOctopus(Enemy):
    name = "land octopus"
    stats = {'hp':  (20,28), 'str': 24, 'dex': 6, 'con': 18, 'psy': 4, 'AC':8, 'exp': 12}
    drops = (Tentacle, 0.5)
    graphic = "Octopus"
    descr = "A giant land octopus from the north. Wondrous creature, and a local delicacy." 
    arms = 4
    hand_name = "tentacle"
    hand_type = Entity.TENTACLES

class StripedWeasel(Enemy):
    name = "striped weasel"
    stats = {'hp':  (8,13), 'str': 13, 'dex': 6, 'con': 10, 'psy': 2, 'AC':4, 'exp': 8}
    drops = (Tentacle, 0.25)
    graphic = "skunk2"
    descr = "A highly vicious animal from the wastes. They have massively expandable jaws, and can take enormous bites out of anything."
    poison_melee = 0.05
    hand_name = "claw"
    hand_type = Entity.NO_FINGERS

class FoulHopper(Enemy):
    name = "foul hopper"
    stats = {'hp':  (5,10), 'str': 5, 'dex': 8, 'con': 6, 'psy': 0, 'AC':2, 'exp': 4}
    drops = (Tentacle, 0.15)
    graphic = "hopper2"
    descr = "This large, dextrous insect feeds on blood. Unthreatening, but irritating."  
    poison_melee = 0.20
    hand_name = "tarsal claw"
    feet_name = "tarsomeres"
    hand_type = Entity.NO_FINGERS


all_enemies = [e for e in globals().values()
               if isinstance(e, type) and Enemy in e.__mro__ and e is not Enemy]
