import pygame
import os

sound_cache = {}
sfxpath = ""

def load_sounds(directory):
    global sfxpath
    sfxpath = directory
#    return
    for f in os.listdir(directory):
        splitname = os.path.splitext(f)
        if splitname[1] not in (".wav", ".ogg"):
            print "skipping load of", f
            continue
        try:
            print "loading " + os.path.join(directory, f)
            sound = pygame.mixer.Sound(os.path.join(directory, f))
        except Exception:
            print "error"
            sound = None
        if not sound:
            print "loading failed:", os.path.join(directory, f)
        else:
            print "loaded as", splitname[0]
            sound_cache[splitname[0]] = sound

def play(name):
    sound = sound_cache.get(name)
    # fname = os.path.join(sfxpath, name + ".ogg")
    # sound = pygame.mixer.Sound(fname)
    if not sound:
        print name + " not found"
        return
    print name, sound.get_length(), "s"
    sound.play()
        
